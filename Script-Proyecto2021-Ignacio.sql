CREATE DATABASE WSTATS;
USE WSTATS;

CREATE TABLE if NOT EXISTS jugadores(
idJugador int AUTO_INCREMENT PRIMARY KEY NOT null,
nombre varchar(50) NOT NULL,
apellidos varchar(150) NOT NULL,
dni VARCHAR(11) NOT NULL,
fechanacimiento DATE,
correo VARCHAR(50) NOT NULL UNIQUE,
posicion VARCHAR(50) NOT NULL,
pass VARCHAR(50) NOT NULL,
tarifa varchar(50),
foto LONGBLOB,
idEquipo INT NOT NULL,
idClub INT NOT NULL,
idEntrenador INT NOT NULL);
--
CREATE TABLE if NOT EXISTS entrenadores (
idEntrenador int AUTO_INCREMENT PRIMARY KEY NOT null,
nombre varchar(50) NOT NULL,
apellidos varchar(150) NOT NULL,
fechanacimiento DATE,
correo VARCHAR(50) NOT NULL UNIQUE,
pass VARCHAR(50) NOT NULL,
foto longblob,
idClub INT NOT NULL);
--
CREATE TABLE IF NOT EXISTS clubs(
idClub int AUTO_INCREMENT PRIMARY KEY NOT NULL,
nombre VARCHAR(50) NOT NULL,
ciudad VARCHAR(50) NOT NULL,
correo VARCHAR(50) NOT NULL UNIQUE,
pass VARCHAR(50) NOT NULL,
foto longblob,
direccion VARCHAR(50) NOT NULL
);
--
CREATE TABLE if NOT EXISTS aficionados(
idAficionado int AUTO_INCREMENT PRIMARY KEY NOT null,
nombre varchar(50) NOT NULL,
apellidos varchar(150) NOT NULL,
ciudad VARCHAR(50) NOT NULL,
correo VARCHAR(50) NOT NULL UNIQUE,
pass VARCHAR(50) NOT NULL,
foto longblob,
fechanacimiento date);
--
CREATE TABLE IF NOT EXISTS padres(
idPadre int AUTO_INCREMENT PRIMARY KEY NOT null,
nombre varchar(50) NOT NULL,
apellidos varchar(150) NOT NULL,
ciudad VARCHAR(50) NOT NULL,
hijo VARCHAR(50) NOT NULL,
correo VARCHAR(50) NOT NULL UNIQUE,
pass VARCHAR(50) NOT NULL,
foto longblob,
fechanacimiento date);
--
CREATE TABLE IF NOT EXISTS estadisticas(
idClub INT NOT null,
idJugador INT UNIQUE, 
lanzamientos int NOT NULL,
goles INT NOT null
);
--
CREATE TABLE if NOT EXISTS equipos(
idEquipo INT AUTO_INCREMENT PRIMARY KEY,
idClub INT NOT NULL,
nombreEquipo VARCHAR(50) NOT NULL
);
--
CREATE TABLE if NOT EXISTS horarios(
idHorario INT AUTO_INCREMENT PRIMARY KEY,
idClub INT NOT NULL,
idEquipo INT NOT NULL,
diaDeLaSemana VARCHAR(20) NOT NULL,
horaInicio VARCHAR(10) NOT NULL,
horaFin VARCHAR(10) NOT NULL,
ubicacion VARCHAR(100) NOT NULL,
urlUbicacion LONGTEXT
);
--
CREATE TABLE if NOT EXISTS noticias(
idNoticia INT AUTO_INCREMENT PRIMARY KEY,
idClub INT NOT NULL,
titulo VARCHAR(50) NOT NULL,
descripcion VARCHAR(50) NOT NULL,
fecha DATE NOT NULL,
texto LONGTEXT
);
--
CREATE TABLE if NOT EXISTS registro(
idregistro INT AUTO_INCREMENT PRIMARY KEY,
tablaCambiada VARCHAR(60) NOT NULL,
fechaCambio DATE NOT NULL,
action VARCHAR(50) NOT NULL);

--
ALTER TABLE equipos
	ADD FOREIGN KEY (idClub) REFERENCES clubs(idClub);

--
ALTER TABLE noticias
   ADD FOREIGN KEY (idClub) REFERENCES clubs(idClub);
--
ALTER TABLE horarios
	ADD FOREIGN KEY (idEquipo) REFERENCES equipos(idEquipo),
   ADD FOREIGN KEY (idClub) REFERENCES clubs(idClub);
--	
alter table jugadores
	ADD FOREIGN KEY (idEntrenador) REFERENCES entrenadores(idEntrenador),
	ADD FOREIGN KEY (idEquipo) REFERENCES equipos(idEquipo),
   ADD FOREIGN KEY (idClub) REFERENCES clubs(idClub);
--
alter table entrenadores
    add foreign key (idClub) references clubs(idClub);
--
/*
delimiter ||
create function existeCodigoInstructor(f_codigo varchar(40))
returns bit
begin
	declare i int;
    set i = 0;
    while ( i < (select max(idinstructor) from instructores)) do
    if  ((select codigoInstructor from instructores where idinstructor = (i+1)) like f_codigo) then return 1;
    end if;
    set i = i + 1;
    end while;
    return 0;
END; 
||
delimiter;
--
delimiter ||
create function existeDniSocio(f_dni varchar(50))
returns bit
begin
	declare i int;
    set i = 0;
    while ( i < (select max(idsocio) from socios)) do
    if  ((select dni FROM socios where idsocio = (i+1)) like f_dni) then return 1;
    end if;
    set i = i + 1;
    end while;
    return 0;
end; 
||
delimiter;
*/
--
delimiter ||
CREATE TRIGGER actualizar_registro_jugadores
    BEFORE UPDATE ON jugadores
    FOR EACH ROW 
 INSERT INTO registro
 SET action = 'update',
     tablaCambiada = 'Jugadores',
     fechaCambio = NOW();
||
delimiter;
--
delimiter ||
CREATE TRIGGER insertar_registro_socios
    BEFORE INSERT ON jugadores
    FOR EACH ROW 
 INSERT INTO registro
 SET action = 'insert',
     tablaCambiada = 'Jugadores',
     fechaCambio = NOW();
||
delimiter;
--
delimiter ||
CREATE TRIGGER delete_registro_socios
    BEFORE DELETE ON jugadores
    FOR EACH ROW 
 INSERT INTO registro
 SET action = 'delete',
     tablaCambiada = 'Jugadores',
     fechaCambio = NOW();
||
delimiter;
--
delimiter ||
CREATE TRIGGER update_registro_instructores
    BEFORE UPDATE ON entrenadores
    FOR EACH ROW 
 INSERT INTO registro
 SET action = 'update',
     tablaCambiada = 'Entrenadores',
     fechaCambio = NOW();
||
delimiter;
--
delimiter ||
CREATE TRIGGER insert_registro_instructores
    BEFORE INSERT ON entrenadores
    FOR EACH ROW 
 INSERT INTO registro
 SET action = 'insert',
     tablaCambiada = 'Entrenadores',
     fechaCambio = NOW();
||
delimiter;
--
delimiter ||
CREATE TRIGGER delete_registro_instructores
    BEFORE DELETE ON entrenadores
    FOR EACH ROW 
 INSERT INTO registro
 SET action = 'delete',
     tablaCambiada = 'Entrenadores',
     fechaCambio = NOW();
||

delimiter ||
create function existeCorreoClub(f_correo varchar(50))
returns bit
begin
	declare i int;
    set i = 0;
    while ( i < (select max(idclub) from clubs)) do
    if  ((select correo FROM clubs where idClub = (i+1)) like f_correo) then return 1;
    end if;
    set i = i + 1;
    end while;
    return 0;
end; 
||

delimiter ||
create function existeCorreoEntrenador(f_correo varchar(50))
returns bit
begin
	declare i int;
    set i = 0;
    while ( i < (select max(identrenador) from entrenadores)) do
    if  ((select correo FROM entrenadores where idEntrenador = (i+1)) like f_correo) then return 1;
    end if;
    set i = i + 1;
    end while;
    return 0;
end; 
||

delimiter ||
create function existeCorreoJugador(f_correo varchar(50))
returns bit
begin
	declare i int;
    set i = 0;
    while ( i < (select max(idjugador) from jugadores)) do
    if  ((select correo FROM jugadores where idJugador = (i+1)) like f_correo) then return 1;
    end if;
    set i = i + 1;
    end while;
    return 0;
end; 
||

delimiter ||
create function existeCorreoAficionado(f_correo varchar(50))
returns bit
begin
	declare i int;
    set i = 0;
    while ( i < (select max(idAficionado) from aficionados)) do
    if  ((select correo FROM aficionados where idAficionado = (i+1)) like f_correo) then return 1;
    end if;
    set i = i + 1;
    end while;
    return 0;
end; 
||
