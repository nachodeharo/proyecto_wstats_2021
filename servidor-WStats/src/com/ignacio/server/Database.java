package com.ignacio.server;

import com.ignacio.*;

import java.sql.*;

public class Database {

    private Connection conexion;

    // Método que llama al metodo conectar() para que la base de datos sea funcuonak cuando se instancie
    public Database() throws SQLException {
        conectar();
    }

    // Permite operar con la BBDD
    public void conectar() throws SQLException {
        conexion=null;
        conexion= DriverManager.getConnection("jdbc:mysql://localhost:3306/wstats","root","");

    }

    // Desconecta la conexión por si fuera necesario
    public void desconectar() throws SQLException {
        conexion.close();
        conexion=null;
    }

    public ResultSet comprobarCredenciales(String tabla, String correo, String pass) throws SQLException {
        String consulta="SELECT * FROM " + tabla + " WHERE correo = ? and pass = ?";
        PreparedStatement sentencia = null;
        // Preparo consulta y establezco parametros
        sentencia = conexion.prepareStatement(consulta);
        sentencia.setString(1,correo);
        sentencia.setString(2,pass);

        // Ejecuto la consulta y la guardo en la variable resultado
        ResultSet resultado=sentencia.executeQuery();

        return resultado;
    }

    public void insertarAficionado(Aficionado aficionado) throws SQLException {
        String consulta ="INSERT INTO aficionados (nombre, apellidos, ciudad, correo, pass, foto, fechanacimiento) VALUES (" +
                "?, ?, ?, ?, ?, ?, ?)";
        PreparedStatement sentencia = null;
        sentencia = conexion.prepareStatement(consulta);

        sentencia.setString(1,aficionado.getNombre());
        sentencia.setString(2, aficionado.getApellidos());
        sentencia.setString(3,aficionado.getCiudad());
        sentencia.setString(4, aficionado.getCorreo());
        sentencia.setString(5, aficionado.getPass());
        sentencia.setBlob(6, aficionado.getFoto());
        sentencia.setDate(7, aficionado.getFechanacimiento());

        System.out.println(consulta);
        sentencia.executeUpdate();

    }

    public void insertarClub(Club club) throws SQLException {
        String consulta ="INSERT INTO clubs (nombre, ciudad, correo, pass, telefono, foto, direccion) VALUES (?, ?, ?, ?, ?, ?, ?)";
        PreparedStatement sentencia = null;
        sentencia = conexion.prepareStatement(consulta);

        sentencia.setString(1,club.getNombre());
        sentencia.setString(2, club.getCiudad());
        sentencia.setString(3,club.getCorreo());
        sentencia.setString(4, club.getPass());
        sentencia.setString(5, String.valueOf(club.getTelefono()));
        Blob blob = new javax.sql.rowset.serial.SerialBlob(club.getFoto());
        sentencia.setBlob(6, blob);
        sentencia.setString(7, club.getDireccion());

        sentencia.executeUpdate();
    }

    public boolean comprobarCorreoClub(Club club) {
        String salesConsult = "SELECT existeCorreoClub(?)";
        PreparedStatement function;
        boolean correoExists = false;
        try {
            function = conexion.prepareStatement(salesConsult);
            function.setString(1, club.getCorreo());
            ResultSet rs = function.executeQuery();
            rs.next();

            correoExists = rs.getBoolean(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return correoExists;
    }

    public boolean comprobarCorreoAficionado(Aficionado aficionado) {
        String salesConsult = "SELECT existeCorreoAficionado(?)";
        PreparedStatement function;
        boolean correoExists = false;
        try {
            function = conexion.prepareStatement(salesConsult);
            function.setString(1, aficionado.getCorreo());
            ResultSet rs = function.executeQuery();
            rs.next();

            correoExists = rs.getBoolean(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        System.out.println("Comprobando si existe: " + correoExists);
        return correoExists;

    }


    public void modificarClub(Club club) throws SQLException {
        String sentenciaSql = "UPDATE clubs SET nombre = ?, ciudad = ?, correo = ?, pass = ?, telefono = ?, foto = ? " +
                "WHERE idClub = ?";
        PreparedStatement sentencia = null;

        sentencia = conexion.prepareStatement(sentenciaSql);
        sentencia.setString(1, club.getNombre());
        sentencia.setString(2, club.getCiudad());
        sentencia.setString(3, club.getCorreo());
        sentencia.setString(4, club.getPass());
        sentencia.setInt(5, club.getTelefono());
        Blob blob = new javax.sql.rowset.serial.SerialBlob(club.getFoto());
        sentencia.setBlob(6, blob);
        sentencia.setInt(7, club.getIdClub());
        sentencia.executeUpdate();

    }

    public ResultSet sacarEquiposClubs(int idClub) throws SQLException {
        String consulta="SELECT * FROM equipos WHERE idClub = ?";
        PreparedStatement sentencia = null;
        // Preparo consulta y establezco parametros
        sentencia = conexion.prepareStatement(consulta);
        sentencia.setInt(1,idClub);

        // Ejecuto la consulta y la guardo en la variable resultado
        ResultSet resultado=sentencia.executeQuery();

        return resultado;
    }

    public int sacarEntrenadoresDeUnClub(int idClub) throws  SQLException {
        String consulta="SELECT count(*) as total FROM entrenadores WHERE idClub = ?";
        PreparedStatement sentencia = null;
        // Preparo consulta y establezco parametros
        sentencia = conexion.prepareStatement(consulta);
        sentencia.setInt(1,idClub);

        // Ejecuto la consulta y la guardo en la variable resultado
        ResultSet resultado=sentencia.executeQuery();

        resultado.next();
        return resultado.getInt(1);
    }
    public int sacarJugadoresDeUnClub(int idClub) throws  SQLException {
        String consulta="SELECT count(*) as total FROM jugadores WHERE idClub = ?";
        PreparedStatement sentencia = null;
        // Preparo consulta y establezco parametros
        sentencia = conexion.prepareStatement(consulta);
        sentencia.setInt(1,idClub);

        // Ejecuto la consulta y la guardo en la variable resultado
        ResultSet resultado=sentencia.executeQuery();

        resultado.next();
        return resultado.getInt(1);
    }

    public void insertarEquipo(String recibido, int idClub) throws SQLException {
        String consulta ="INSERT INTO equipos (idClub, nombreEquipo) VALUES (?, ?)";
        PreparedStatement sentencia = null;
        sentencia = conexion.prepareStatement(consulta);

        sentencia.setInt(1, idClub);
        sentencia.setString(2, recibido);

        sentencia.executeUpdate();
    }

    public void eliminarEquipo(String recibido) throws SQLException {
        String consulta ="DELETE FROM equipos WHERE idEquipo = ?";
        PreparedStatement sentencia = null;
        sentencia = conexion.prepareStatement(consulta);

        sentencia.setString(1, recibido);

        sentencia.executeUpdate();
    }

    public ResultSet sacarJugadoresClub(int idClub) throws SQLException {
        String consulta="SELECT * FROM jugadores WHERE idClub = ? ORDER BY idEquipo";
        PreparedStatement sentencia = null;
        // Preparo consulta y establezco parametros
        sentencia = conexion.prepareStatement(consulta);
        sentencia.setInt(1,idClub);

        // Ejecuto la consulta y la guardo en la variable resultado
        ResultSet resultado=sentencia.executeQuery();

        return resultado;
    }

    public void eliminarJugador(String recibido) throws SQLException {
        String consulta ="DELETE FROM jugadores WHERE idJugador = ?";
        PreparedStatement sentencia = null;
        sentencia = conexion.prepareStatement(consulta);

        sentencia.setString(1, recibido);

        sentencia.executeUpdate();
    }

    public ResultSet sacarEntrenadoresClub(int idClub) throws SQLException {
        String consulta="SELECT * FROM entrenadores WHERE idClub = ?";
        PreparedStatement sentencia = null;
        // Preparo consulta y establezco parametros
        sentencia = conexion.prepareStatement(consulta);
        sentencia.setInt(1,idClub);

        // Ejecuto la consulta y la guardo en la variable resultado
        ResultSet resultado=sentencia.executeQuery();

        return resultado;
    }

    public void actualizarJugador(Jugador jugador) throws SQLException {
        String sentenciaSql = "UPDATE jugadores SET nombre = ?, apellidos = ?, dni = ?, fechanacimiento = ?, correo = ?, " +
                "posicion = ?, pass = ?, tarifa = ?, foto = ?, idEquipo = ?, idClub = ?, idEntrenador = ? " +
                "WHERE idJugador = ?";
        PreparedStatement sentencia = null;

        sentencia = conexion.prepareStatement(sentenciaSql);
        sentencia.setString(1, jugador.getNombre());
        sentencia.setString(2, jugador.getApellidos());
        sentencia.setString(3, jugador.getDni());
        sentencia.setDate(4,jugador.getFechanacimiento());
        sentencia.setString(5, jugador.getCorreo());
        sentencia.setString(6, jugador.getPosicion());
        sentencia.setString(7, jugador.getPass());
        sentencia.setString(8, jugador.getTarifa());
        Blob blob = null;
        if (jugador.getFoto()!=null){
             blob = new javax.sql.rowset.serial.SerialBlob(jugador.getFoto());
        }
        sentencia.setBlob(9, blob);
        sentencia.setInt(10, jugador.getIdEquipo());
        sentencia.setInt(11, jugador.getIdClub());
        sentencia.setInt(12, jugador.getIdEntrenador());
        sentencia.setInt(13, jugador.getIdJugador());
        sentencia.executeUpdate();
    }

    public void insertarJugador(Jugador jugador) throws SQLException {
        String sentenciaSql = "INSERT INTO jugadores (nombre, apellidos, dni, fechanacimiento, correo, posicion, pass, tarifa, foto, idEquipo, idClub, idEntrenador) VALUES " +
                "(?,?,?,?,?,?,?,?,?,?,?,?)";
        PreparedStatement sentencia = null;

        sentencia = conexion.prepareStatement(sentenciaSql);
        sentencia.setString(1, jugador.getNombre());
        sentencia.setString(2, jugador.getApellidos());
        sentencia.setString(3, jugador.getDni());
        sentencia.setDate(4, jugador.getFechanacimiento());
        sentencia.setString(5, jugador.getCorreo());
        sentencia.setString(6, jugador.getPosicion());
        sentencia.setString(7, jugador.getPass());
        sentencia.setString(8, jugador.getTarifa());
        Blob blob = null;
        if (jugador.getFoto()!=null){
            blob = new javax.sql.rowset.serial.SerialBlob(jugador.getFoto());
        }
        sentencia.setBlob(9, blob);
        sentencia.setInt(10, jugador.getIdEquipo());
        sentencia.setInt(11, jugador.getIdClub());
        sentencia.setInt(12, jugador.getIdEntrenador());
        sentencia.executeUpdate();
    }

    public void eliminarEntrenador(String idClub) throws SQLException {
        String consulta ="DELETE FROM entrenadores WHERE idEntrenador = ?";
        PreparedStatement sentencia = null;
        sentencia = conexion.prepareStatement(consulta);

        sentencia.setString(1, idClub);

        sentencia.executeUpdate();
    }

    public void actualizarEntrenador(Entrenador entrenador) throws SQLException {
        String sentenciaSql = "UPDATE entrenadores SET nombre = ?, apellidos = ?, fechanacimiento = ?, correo = ?, " +
                "foto = ? WHERE idEntrenador = ?";
        PreparedStatement sentencia = null;
        sentencia = conexion.prepareStatement(sentenciaSql);

        sentencia.setString(1, entrenador.getNombre());
        sentencia.setString(2, entrenador.getApellidos());
        sentencia.setDate(3, entrenador.getFechanacimiento());
        sentencia.setString(4, entrenador.getCorreo());
        sentencia.setString(5, null);
        sentencia.setInt(6, entrenador.getIdEntrenador());

        sentencia.executeUpdate();
    }

    public void insertarEntrenador(Entrenador entrenador) throws SQLException {
        String sentenciaSql = "INSERT INTO entrenadores (nombre, apellidos, fechanacimiento, correo, pass, foto, idClub) VALUES (?, ?, ?, ?, ?, ?, ?)";
        PreparedStatement sentencia = null;
        sentencia = conexion.prepareStatement(sentenciaSql);

        sentencia.setString(1, entrenador.getNombre());
        sentencia.setString(2, entrenador.getApellidos());
        sentencia.setDate(3, entrenador.getFechanacimiento());
        sentencia.setString(4, entrenador.getCorreo());
        sentencia.setString(5, entrenador.getPass());
        sentencia.setString(6, null);
        sentencia.setInt(7, entrenador.getIdClub());
        sentencia.executeUpdate();
    }

    public ResultSet sacarHorariosClub(int idClub) throws SQLException {
        String consulta="SELECT * FROM horarios WHERE idClub = ? ORDER BY diaDeLaSemana";
        PreparedStatement sentencia = null;
        // Preparo consulta y establezco parametros
        sentencia = conexion.prepareStatement(consulta);
        sentencia.setInt(1,idClub);

        // Ejecuto la consulta y la guardo en la variable resultado
        ResultSet resultado=sentencia.executeQuery();

        return resultado;
    }

    public void insertarHorarioClub(Horario horario) throws SQLException {
        String sentenciaSql = "INSERT INTO horarios (idClub, idEquipo, diaDeLaSemana, horaInicio, horaFin, ubicacion, urlUbicacion) VALUES (?, ?, ?, ?, ?, ?, ?)";
        PreparedStatement sentencia = null;
        sentencia = conexion.prepareStatement(sentenciaSql);

        sentencia.setInt(1, horario.getIdClub());
        sentencia.setInt(2, horario.getIdEquipo());
        sentencia.setString(3, horario.getDiaDeLaSemana());
        sentencia.setString(4, horario.getHoraInicio());
        sentencia.setString(5, horario.getHoraFin());
        sentencia.setString(6, horario.getUbicacion());
        sentencia.setString(7, horario.getUrlUbicacion());
        sentencia.executeUpdate();
    }

    public void eliminarHorario(Horario horarioClub) throws SQLException {
        String consulta ="DELETE FROM horarios WHERE idHorario = ?";
        PreparedStatement sentencia = null;
        sentencia = conexion.prepareStatement(consulta);

        sentencia.setInt(1, horarioClub.getIdHorario());

        sentencia.executeUpdate();
    }

    public ResultSet sacarNoticiasClub(int idClub) throws SQLException {
        String consulta="SELECT * FROM noticias WHERE idClub = ? ORDER BY fecha DESC";
        PreparedStatement sentencia = null;
        // Preparo consulta y establezco parametros
        sentencia = conexion.prepareStatement(consulta);
        sentencia.setInt(1,idClub);

        // Ejecuto la consulta y la guardo en la variable resultado
        ResultSet resultado=sentencia.executeQuery();

        return resultado;
    }

    public ResultSet sacarTodasLasNoticias() throws SQLException {
        String consulta="SELECT * FROM noticias ORDER BY fecha DESC";
        PreparedStatement sentencia = null;
        // Preparo consulta y establezco parametros
        sentencia = conexion.prepareStatement(consulta);

        // Ejecuto la consulta y la guardo en la variable resultado
        ResultSet resultado=sentencia.executeQuery();

        return resultado;
    }

    public void eliminarNoticiaClub(Noticia noticia) throws SQLException {
        String consulta ="DELETE FROM noticias WHERE idNoticia = ?";
        PreparedStatement sentencia = null;
        sentencia = conexion.prepareStatement(consulta);

        sentencia.setInt(1, noticia.getIdNoticia());

        sentencia.executeUpdate();
    }

    public void modificarNoticiaClub(Noticia noticia) throws SQLException{
        String sentenciaSql = "UPDATE noticias SET idClub = ?, titulo = ?, descripcion = ?, fecha = ?, " +
                "texto = ? WHERE idNoticia = ?";
        PreparedStatement sentencia = null;
        sentencia = conexion.prepareStatement(sentenciaSql);

        sentencia.setInt(1, noticia.getIdClub());
        sentencia.setString(2, noticia.getTitulo());
        sentencia.setString(3, noticia.getDescripcion());
        sentencia.setDate(4, noticia.getFecha());
        sentencia.setString(5, noticia.getTexto());
        sentencia.setInt(6, noticia.getIdNoticia());

        sentencia.executeUpdate();
    }

    public void nuevaNoticiaClub(Noticia noticia) throws SQLException{
        String sentenciaSql = "INSERT INTO noticias (idNoticia, idCLub, titulo, descripcion, fecha, texto) VALUES (?, ?, ?, ?, ?, ?)";
        PreparedStatement sentencia = null;
        sentencia = conexion.prepareStatement(sentenciaSql);

        sentencia.setInt(1, noticia.getIdNoticia());
        sentencia.setInt(2, noticia.getIdClub());
        sentencia.setString(3, noticia.getTitulo());
        sentencia.setString(4, noticia.getDescripcion());
        sentencia.setDate(5, noticia.getFecha());
        sentencia.setString(6, noticia.getTexto());
        sentencia.executeUpdate();
    }

}
