package com.ignacio.server;

import com.google.gson.Gson;
import com.ignacio.*;
import com.nacho.WHashCrypt;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.sql.Blob;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * Clase que proporciona un hilo de ejecución a cada conexión que es aceptada
 * por el servidor
 */
public class ServidorHilo extends Thread {
    private Socket socket;
    private DataOutputStream salida;
    private DataInputStream entrada;
    private int idSessio;
    private String recibido;


    /**
     * Constructor de la clase
     *
     * @param socket socket de conexión con el cliente
     * @param id     int que representa el id de sesion asignado por el servidor
     */
    public ServidorHilo(Socket socket, int id) {
        this.socket = socket;
        this.idSessio = id;
        try {
            //Se crean los canales de comunicación con el cliente
            salida = new DataOutputStream(socket.getOutputStream());
            entrada = new DataInputStream(socket.getInputStream());
        } catch (IOException ex) {
            Logger.getLogger(ServidorHilo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Método que proporciona
     */
    public void desconnectar() {
        try {
            //Cierro todo
            salida.close();
            entrada.close();
            socket.close();
        } catch (IOException ex) {
            Logger.getLogger(ServidorHilo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Método que activa el código a ejecutar cuando se inicia el hilo
     */
    @Override
    public void run() {
        String accion = "";
        Gson g = new Gson();
        Database db;
        WHashCrypt wp = new WHashCrypt();
        ResultSet resultado = null;
        boolean llave = false;

        try {
            db = new Database();
            //obtiene el mensaje del cliente y lo descrifra y lo pasa a la clase correspondiente
            recibido = entrada.readUTF();
            recibido = wp.desCifrar(recibido, "123456789123456789");
            Usuario user = g.fromJson(recibido, Usuario.class);
            System.out.println("Nuevo registro -> " + user);
            if (user.getTipo().equalsIgnoreCase("registro")) {
                System.out.println("Leo y descifro");
                recibido = entrada.readUTF();
                recibido = wp.desCifrar(recibido, "123456789123456789");
                Aficionado aficionado = g.fromJson(recibido, Aficionado.class);
                // LO HASHEO PARA NO SABER LA PASS
                aficionado.setPass(wp.hashear(aficionado.getPass()));
                System.out.println(aficionado);
                if (!db.comprobarCorreoAficionado(aficionado)) {
                    db.insertarAficionado(aficionado);
                    System.out.println("Nuevo aficionado:");
                    System.out.println(aficionado);
                    salida.writeUTF("correcto");
                } else {
                    salida.writeUTF("error");
                }
            } else {
                DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
                switch (user.getTipo()) {
                    case "clubs":
                    case "entrenadores":
                    case "jugadores":
                    case "aficionados":
                        resultado = db.comprobarCredenciales(user.getTipo(), user.getCorreo(), user.getPass());
                        if (resultado.next() && resultado != null) {
                            String cifrado = null;
                            salida.writeUTF("si");
                            switch (user.getTipo()) {
                                case "clubs":
                                    System.out.println("Nuevo club");
                                    try{
                                        Club club = new Club();
                                        int idClub = resultado.getInt("idClub");
                                        club.setIdClub(idClub);
                                        club.setNombre(resultado.getString("nombre"));
                                        club.setCiudad(resultado.getString("ciudad"));
                                        club.setCorreo(resultado.getString("correo"));
                                        club.setPass(resultado.getString("pass"));
                                        club.setTelefono(resultado.getInt("telefono"));
                                        Blob blob = null;
                                        blob = resultado.getBlob("foto");
                                        int blobLength = 0;
                                        byte[] blobAsBytes = null;
                                        if (blob!=null){
                                            blobLength = (int) blob.length();
                                            blobAsBytes = blob.getBytes(1, blobLength);
                                            blob.free();
                                            //club.setFoto(blobAsBytes);
                                        }else{
                                            club.setFoto(null);
                                        }
                                        club.setDireccion(resultado.getString("direccion"));
                                        club.setNumeroEntrenadores(db.sacarEntrenadoresDeUnClub(idClub));
                                        club.setNumeroJugadores(db.sacarJugadoresDeUnClub(idClub));

                                        System.out.println(club);
                                        //Cifrar y pasar a JSON
                                        cifrado = g.toJson(club);
                                        cifrado = wp.cifrar(cifrado, "123456789123456789");

                                        //Enviar a cliente
                                        salida.writeUTF(cifrado);

                                    }catch (SQLException e){
                                        e.printStackTrace();
                                    }

                                    break;

                                case "entrenadores":
                                    Entrenador entrenador = new Entrenador();
                                    entrenador.setIdEntrenador(resultado.getInt("idEntrenador"));
                                    entrenador.setNombre(resultado.getString("nombre"));
                                    entrenador.setApellidos(resultado.getString("apellidos"));

                                    entrenador.setFechanacimiento(resultado.getDate("fechanacimiento"));

                                    entrenador.setCorreo(resultado.getString("correo"));
                                    entrenador.setPass(resultado.getString("pass"));
                                    //entrenador.setFoto(resultado.getBlob("foto"));
                                    entrenador.setIdClub(resultado.getInt("idClub"));

                                    //Cifrar y pasar a JSON
                                    cifrado = g.toJson(entrenador);
                                    cifrado = wp.cifrar(cifrado, "123456789123456789");
                                    System.out.println(entrenador);
                                    //Enviar a cliente
                                    salida.writeUTF(cifrado);
                                    break;

                                case "jugadores":
                                    Jugador jugador = new Jugador();
                                    jugador.setIdJugador(resultado.getInt("idJugador"));
                                    jugador.setNombre(resultado.getString("nombre"));
                                    jugador.setApellidos(resultado.getString("apellidos"));
                                    jugador.setDni(resultado.getString("dni"));

                                    jugador.setFechanacimiento(resultado.getDate("fechanacimiento"));

                                    jugador.setCorreo(resultado.getString("correo"));
                                    jugador.setPosicion(resultado.getString("posicion"));
                                    jugador.setPass(resultado.getString("pass"));
                                    jugador.setTarifa(resultado.getString("tarifa"));
                                    //jugador.setFoto(resultado.getBlob("foto"));
                                    jugador.setIdEquipo(resultado.getInt("idEquipo"));
                                    jugador.setIdClub(resultado.getInt("idClub"));
                                    jugador.setIdEntrenador(resultado.getInt("idEntrenador"));

                                    System.out.println("SE LOGUEA JUGADOR: \n" + jugador);
                                    //Cifrar y pasar a JSON
                                    cifrado = g.toJson(jugador);
                                    cifrado = wp.cifrar(cifrado, "123456789123456789");
                                    System.out.println(jugador);
                                    //Enviar a cliente
                                    salida.writeUTF(cifrado);
                                    break;

                                case "aficionados":
                                    Aficionado aficionado = new Aficionado();
                                    aficionado.setIdAficionado(resultado.getInt("idAficionado"));
                                    aficionado.setNombre(resultado.getString("nombre"));
                                    aficionado.setApellidos(resultado.getString("apellidos"));
                                    aficionado.setCiudad(resultado.getString("ciudad"));
                                    aficionado.setCorreo(resultado.getString("correo"));
                                    aficionado.setPass(resultado.getString("pass"));
                                    aficionado.setFoto(resultado.getBlob("foto"));
                                    aficionado.setFechanacimiento(resultado.getDate("fechanacimiento"));

                                    //Cifrar y pasar a JSON
                                    cifrado = g.toJson(aficionado);
                                    cifrado = wp.cifrar(cifrado, "123456789123456789");

                                    //Enviar a cliente
                                    salida.writeUTF(cifrado);
                                    break;
                                default:
                                    break;
                            }

                            // ¿Me quedo pidiendo respuesta de cliente?

                            // En función de la respuesta:
                        /*
                                -   Pedir datos de X persona
                                -   Actualizar datos de él mismo
                                -   Pedir lista de datos   --> ArrayList<Jugador> // ArrayList<Entrenador>
                                -   Pedir entrenamientos/horarios
                                -   Pedir noticias
                        */
                        } else {
                            salida.writeUTF("no");
                        }
                        break;

                    case "actualizarClubs":
                        recibido = entrada.readUTF();
                        recibido = wp.desCifrar(recibido, "123456789123456789");
                        Club club = g.fromJson(recibido, Club.class);

                        byte[] bytes = new byte[62100];
                        entrada.read(bytes);
                        club.setFoto(bytes);

                        try{
                            db.modificarClub(club);
                            salida.writeUTF("OK");
                        }catch (SQLException e){
                            salida.writeUTF("ERROR");
                        }
                        break;

                    case "sacarEquiposClub":
                        recibido = entrada.readUTF();
                        recibido = wp.desCifrar(recibido, "123456789123456789");
                        Club club2 = g.fromJson(recibido, Club.class);
                        String strSalida = null;
                        try{
                            ResultSet rs = db.sacarEquiposClubs(club2.getIdClub());
                            ArrayList<Equipo> arrayEquipos = new ArrayList<Equipo>();
                            while (rs.next() && rs != null) {
                                Equipo equipo = new Equipo();
                                equipo.setIdClub(rs.getInt("idClub"));
                                equipo.setIdEquipo(rs.getInt("idEquipo"));
                                equipo.setNombreEquipo(rs.getString("nombreEquipo"));
                                arrayEquipos.add(equipo);
                            }
                            if (arrayEquipos.size() > -1){
                                System.out.println(arrayEquipos);
                                strSalida = wp.cifrar(g.toJson(arrayEquipos), "123456789123456789");
                                salida.writeUTF(strSalida);
                            }else{
                                salida.writeUTF(null);
                            }


                        }catch (SQLException e){
                            e.printStackTrace();
                        }
                        break;

                    case "meterEquiposClub":
                        recibido = entrada.readUTF();
                        recibido = wp.desCifrar(recibido, "123456789123456789");
                        Club club3 = g.fromJson(recibido, Club.class);

                        recibido = entrada.readUTF();

                        try{
                            db.insertarEquipo(recibido, club3.getIdClub());

                            ResultSet rs = db.sacarEquiposClubs(club3.getIdClub());
                            ArrayList<Equipo> arrayEquipos = new ArrayList<Equipo>();
                            while (rs.next() && rs != null) {
                                Equipo equipo = new Equipo();
                                equipo.setIdClub(rs.getInt("idClub"));
                                equipo.setIdEquipo(rs.getInt("idEquipo"));
                                equipo.setNombreEquipo(rs.getString("nombreEquipo"));
                                arrayEquipos.add(equipo);
                            }
                            if (arrayEquipos.size() > -1){
                                System.out.println("ENVIO: " + arrayEquipos);
                                strSalida = wp.cifrar(g.toJson(arrayEquipos), "123456789123456789");
                                salida.writeUTF(strSalida);
                            }else{
                                salida.writeUTF(null);
                            }


                        }catch (SQLException e){
                            e.printStackTrace();
                        }

                        break;
                    case "eliminarEquiposClub":
                        recibido = entrada.readUTF();
                        recibido = wp.desCifrar(recibido, "123456789123456789");
                        Club club4 = g.fromJson(recibido, Club.class);

                        recibido = entrada.readUTF();

                        try{
                        db.eliminarEquipo(recibido);

                        ResultSet rs = db.sacarEquiposClubs(club4.getIdClub());
                        ArrayList<Equipo> arrayEquipos = new ArrayList<Equipo>();
                        while (rs.next() && rs != null) {
                            Equipo equipo = new Equipo();
                            equipo.setIdClub(rs.getInt("idClub"));
                            equipo.setIdEquipo(rs.getInt("idEquipo"));
                            equipo.setNombreEquipo(rs.getString("nombreEquipo"));
                            arrayEquipos.add(equipo);
                        }
                        if (arrayEquipos.size() > -1){
                            strSalida = wp.cifrar(g.toJson(arrayEquipos), "123456789123456789");
                            salida.writeUTF(strSalida);
                        }else{
                            salida.writeUTF(null);
                        }


                    }catch (SQLException e){
                        e.printStackTrace();
                    }
                        break;

                    case "sacarJugadoresClub":
                        recibido = entrada.readUTF();
                        recibido = wp.desCifrar(recibido, "123456789123456789");
                        Club club5 = g.fromJson(recibido, Club.class);
                        String strSalida2 = null;
                        try{
                            ResultSet rs = db.sacarJugadoresClub(club5.getIdClub());
                            ArrayList<Jugador> arrayJugadores = new ArrayList<Jugador>();
                            while (rs.next() && rs != null) {
                                Jugador jugador = new Jugador();
                                jugador.setIdJugador(rs.getInt("idJugador"));
                                jugador.setNombre(rs.getString("nombre"));
                                jugador.setApellidos(rs.getString("apellidos"));
                                jugador.setDni(rs.getString("dni"));
                                //fechas
                                jugador.setFechanacimiento(rs.getDate("fechanacimiento"));
                                //fin fechas
                                jugador.setCorreo(rs.getString("correo"));
                                jugador.setPosicion(rs.getString("posicion"));
                                jugador.setPass(rs.getString("pass"));
                                jugador.setTarifa(rs.getString("tarifa"));
                                jugador.setFoto(null);
                                jugador.setIdClub(rs.getInt("idClub"));
                                jugador.setIdEquipo(rs.getInt("idEquipo"));
                                jugador.setIdEntrenador(rs.getInt("idEntrenador"));
                                arrayJugadores.add(jugador);
                            }
                            if (arrayJugadores.size() > -1){
                                //System.out.println(arrayJugadores);
                                strSalida2 = wp.cifrar(g.toJson(arrayJugadores), "123456789123456789");
                                salida.writeUTF(strSalida2);
                            }else{
                                salida.writeUTF(null);
                            }


                        }catch (SQLException e){
                            e.printStackTrace();
                        }
                        break;

                    case "eliminarJugadoresClub":
                        recibido = entrada.readUTF();
                        recibido = wp.desCifrar(recibido, "123456789123456789");
                        Club club6 = g.fromJson(recibido, Club.class);

                        recibido = entrada.readUTF();

                        try{
                            db.eliminarJugador(recibido);
                            ResultSet rs = db.sacarJugadoresClub(club6.getIdClub());
                            ArrayList<Jugador> arrayJugadores = new ArrayList<Jugador>();
                            while (rs.next() && rs != null) {
                                Jugador jugador = new Jugador();
                                jugador.setIdJugador(rs.getInt("idJugador"));
                                jugador.setNombre(rs.getString("nombre"));
                                jugador.setApellidos(rs.getString("apellidos"));
                                jugador.setDni(rs.getString("dni"));
                                //fechas
                                jugador.setFechanacimiento(rs.getDate("fechanacimiento"));
                                //fin fechas
                                jugador.setCorreo(rs.getString("correo"));
                                jugador.setPosicion(rs.getString("posicion"));
                                jugador.setPass(rs.getString("pass"));
                                jugador.setTarifa(rs.getString("tarifa"));
                                jugador.setFoto(null);
                                jugador.setIdClub(rs.getInt("idClub"));
                                jugador.setIdEquipo(rs.getInt("idEquipo"));
                                jugador.setIdEntrenador(rs.getInt("idEntrenador"));
                                arrayJugadores.add(jugador);
                            }
                            if (arrayJugadores.size() > -1){
                                //System.out.println(arrayJugadores);
                                strSalida2 = wp.cifrar(g.toJson(arrayJugadores), "123456789123456789");
                                salida.writeUTF(strSalida2);
                            }else{
                                salida.writeUTF(null);
                            }


                        }catch (SQLException e){
                            e.printStackTrace();
                        }
                        break;

                    case "sacarEquipoEntrenadorClub":
                        recibido = entrada.readUTF();
                        recibido = wp.desCifrar(recibido, "123456789123456789");
                        Club club7 = g.fromJson(recibido, Club.class);

                        try{
                            ResultSet rs = db.sacarEquiposClubs(club7.getIdClub());
                            ArrayList<Equipo> listaEquipo = new ArrayList<>();
                            ArrayList<Entrenador> listaEntrenadores = new ArrayList<>();

                            //Envio los equipos
                            while (rs.next() && rs != null) {
                                Equipo equipo2 = new Equipo();
                                equipo2.setIdClub(rs.getInt("idClub"));
                                equipo2.setIdEquipo(rs.getInt("idEquipo"));
                                equipo2.setNombreEquipo(rs.getString("nombreEquipo"));
                                listaEquipo.add(equipo2);
                            }
                            if (listaEquipo.size() > -1){
                                System.out.println(listaEquipo);
                                strSalida = wp.cifrar(g.toJson(listaEquipo), "123456789123456789");
                                salida.writeUTF(strSalida);
                            }else{
                                salida.writeUTF(null);
                            }

                            rs = db.sacarEntrenadoresClub(club7.getIdClub());

                            //Envio los entrenadores
                            //Envio los equipos
                            while (rs.next() && rs != null) {
                                Entrenador entrenador = new Entrenador();
                                entrenador.setIdClub(rs.getInt("idClub"));
                                entrenador.setIdEntrenador(rs.getInt("idEntrenador"));
                                entrenador.setNombre(rs.getString("nombre"));
                                entrenador.setApellidos(rs.getString("apellidos"));

                                //LA FECHA PARSEADA
                                entrenador.setFechanacimiento(rs.getDate("fechanacimiento"));

                                entrenador.setCorreo(rs.getString("correo"));
                                entrenador.setPass(rs.getString("pass"));
                                //Foto de blob
                                Blob blob = null;
                                blob = rs.getBlob("foto");
                                int blobLength = 0;
                                byte[] blobAsBytes = null;
                                if (blob!=null){
                                    blobLength = (int) blob.length();
                                    blobAsBytes = blob.getBytes(1, blobLength);
                                    blob.free();
                                    entrenador.setFoto(blobAsBytes);
                                }else{
                                    entrenador.setFoto(null);
                                }

                                entrenador.setIdClub(rs.getInt("idClub"));

                                listaEntrenadores.add(entrenador);
                            }
                            if (listaEquipo.size() > -1){
                                System.out.println(listaEntrenadores);
                                strSalida = wp.cifrar(g.toJson(listaEntrenadores), "123456789123456789");
                                salida.writeUTF(strSalida);
                            }else{
                                salida.writeUTF(null);
                            }

                        }catch (SQLException e){

                        }
                        break;
                    case "actualizarJugador":
                        recibido = entrada.readUTF();
                        recibido = wp.desCifrar(recibido, "123456789123456789");
                        Jugador jugador = g.fromJson(recibido, Jugador.class);

                        try{
                            db.actualizarJugador(jugador);
                        }catch (SQLException e){
                            e.printStackTrace();
                        }
                        break;

                    case "nuevoJugador":
                        recibido = entrada.readUTF();
                        recibido = wp.desCifrar(recibido, "123456789123456789");
                        Jugador jugador2 = g.fromJson(recibido, Jugador.class);

                        try{
                            db.insertarJugador(jugador2);
                        }catch (SQLException e){
                            e.printStackTrace();
                        }
                        break;

                    case "sacarEntrenadoresClub":
                        recibido = entrada.readUTF();
                        recibido = wp.desCifrar(recibido, "123456789123456789");
                        Club clubEntrenador = g.fromJson(recibido, Club.class);
                        String strSalid = null;
                        ArrayList<Entrenador> arrayEntrenadores = new ArrayList<>();
                        try{
                            ResultSet rs = db.sacarEntrenadoresClub(clubEntrenador.getIdClub());
                            while (rs.next() && rs != null) {
                                Entrenador entrenador = new Entrenador();
                                entrenador.setIdClub(rs.getInt("idClub"));
                                entrenador.setIdEntrenador(rs.getInt("idEntrenador"));
                                entrenador.setNombre(rs.getString("nombre"));
                                entrenador.setApellidos(rs.getString("apellidos"));

                                //LA FECHA PARSEADA
                                //System.out.println(resultado.getString("fechanacimiento"));
                                entrenador.setFechanacimiento(rs.getDate("fechanacimiento"));

                                entrenador.setCorreo(rs.getString("correo"));
                                entrenador.setPass(rs.getString("pass"));
                                //Foto de blob
                                Blob blob = null;
                                blob = rs.getBlob("foto");
                                int blobLength = 0;
                                byte[] blobAsBytes = null;
                                if (blob!=null){
                                    blobLength = (int) blob.length();
                                    blobAsBytes = blob.getBytes(1, blobLength);
                                    blob.free();
                                    entrenador.setFoto(blobAsBytes);
                                }else{
                                    entrenador.setFoto(null);
                                }

                                entrenador.setIdClub(rs.getInt("idClub"));

                                arrayEntrenadores.add(entrenador);
                            }
                            if (arrayEntrenadores.size() > -1){
                                System.out.println(g.toJson(arrayEntrenadores));
                                strSalida = wp.cifrar(g.toJson(arrayEntrenadores), "123456789123456789");
                                System.out.println(wp.desCifrar(strSalida, "123456789123456789"));
                                salida.writeUTF(strSalida);
                            }else{
                                salida.writeUTF(null);
                            }


                        }catch (SQLException e){
                            e.printStackTrace();
                        }
                        break;

                    case "eliminarEntrenadorClub":
                        recibido = entrada.readUTF();
                        recibido = wp.desCifrar(recibido, "123456789123456789");
                        Club club10 = g.fromJson(recibido, Club.class);

                        recibido = entrada.readUTF();

                        try{
                            db.eliminarEntrenador(recibido);
                            ResultSet rs = db.sacarEntrenadoresClub(club10.getIdClub());
                            ArrayList<Entrenador> arrayEntrenador = new ArrayList<Entrenador>();
                            while (rs.next() && rs != null) {
                                Entrenador entrenador = new Entrenador();
                                entrenador.setIdClub(rs.getInt("idClub"));
                                entrenador.setIdEntrenador(rs.getInt("idEntrenador"));
                                entrenador.setNombre(rs.getString("nombre"));
                                entrenador.setApellidos(rs.getString("apellidos"));

                                //LA FECHA PARSEADA
                                entrenador.setFechanacimiento(rs.getDate("fechanacimiento"));

                                entrenador.setCorreo(rs.getString("correo"));
                                entrenador.setPass(rs.getString("pass"));
                                //Foto de blob
                                Blob blob = null;
                                blob = rs.getBlob("foto");
                                int blobLength = 0;
                                byte[] blobAsBytes = null;
                                if (blob!=null){
                                    blobLength = (int) blob.length();
                                    blobAsBytes = blob.getBytes(1, blobLength);
                                    blob.free();
                                    entrenador.setFoto(blobAsBytes);
                                }else{
                                    entrenador.setFoto(null);
                                }

                                entrenador.setIdClub(rs.getInt("idClub"));

                                arrayEntrenador.add(entrenador);
                            }
                            if (arrayEntrenador.size() > -1){
                                System.out.println(g.toJson(arrayEntrenador));
                                strSalida = wp.cifrar(g.toJson(arrayEntrenador), "123456789123456789");
                                salida.writeUTF(strSalida);
                            }else{
                                salida.writeUTF(null);
                            }


                        }catch (SQLException e){
                            System.out.println("NO SE HA PODIDO ELIMINAR");
                            strSalida = wp.cifrar("error","123456789123456789");
                            salida.writeUTF(strSalida);
                        }
                        break;

                    case "actualizarEntrenador":
                        recibido = entrada.readUTF();
                        recibido = wp.desCifrar(recibido, "123456789123456789");
                        Entrenador entrenador = g.fromJson(recibido, Entrenador.class);

                        try{
                            db.actualizarEntrenador(entrenador);
                        }catch (SQLException e){
                            e.printStackTrace();
                        }
                        break;

                    case "nuevoEntrenador":
                        recibido = entrada.readUTF();
                        recibido = wp.desCifrar(recibido, "123456789123456789");
                        Entrenador entrenador1 = g.fromJson(recibido, Entrenador.class);

                        try{
                            db.insertarEntrenador(entrenador1);
                            System.out.println("NUEVO ENTRENADOR : " + entrenador1);
                        }catch (SQLException e){
                            e.printStackTrace();
                        }

                        break;

                    case "sacarHorariosClub":
                        recibido = entrada.readUTF();
                        recibido = wp.desCifrar(recibido, "123456789123456789");
                        Club clubHorario = g.fromJson(recibido, Club.class);
                        String strSalidas = null;
                        ArrayList<Horario> arrayHorarios = new ArrayList<>();
                        try{
                            ResultSet rs = db.sacarHorariosClub(clubHorario.getIdClub());
                            while (rs.next() && rs != null) {
                                Horario horario = new Horario();

                                horario.setIdHorario(rs.getInt("idHorario"));
                                horario.setIdClub(rs.getInt("idClub"));
                                horario.setIdEquipo(rs.getInt("idEquipo"));
                                horario.setDiaDeLaSemana(rs.getString("diaDeLaSemana"));
                                horario.setHoraInicio(rs.getString("horaInicio"));
                                horario.setHoraFin(rs.getString("horaFin"));
                                horario.setUbicacion(rs.getString("ubicacion"));
                                horario.setUrlUbicacion(rs.getString("urlUbicacion"));
                                arrayHorarios.add(horario);
                            }
                            if (arrayHorarios.size() > -1){
                                System.out.println(g.toJson(arrayHorarios));
                                strSalidas = wp.cifrar(g.toJson(arrayHorarios), "123456789123456789");
                                System.out.println(wp.desCifrar(strSalidas, "123456789123456789"));
                                salida.writeUTF(strSalidas);
                            }else{
                                salida.writeUTF(null);
                            }

                            rs = db.sacarEquiposClubs(clubHorario.getIdClub());
                            ArrayList<Equipo> listaEquipo = new ArrayList<Equipo>();
                            while (rs.next() && rs != null) {
                                Equipo equipo2 = new Equipo();
                                equipo2.setIdClub(rs.getInt("idClub"));
                                equipo2.setIdEquipo(rs.getInt("idEquipo"));
                                equipo2.setNombreEquipo(rs.getString("nombreEquipo"));
                                listaEquipo.add(equipo2);
                            }
                            if (listaEquipo.size() > -1){
                                System.out.println(listaEquipo);
                                strSalida = wp.cifrar(g.toJson(listaEquipo), "123456789123456789");
                                salida.writeUTF(strSalida);
                            }else{
                                salida.writeUTF(null);
                            }

                        }catch (SQLException e){
                            e.printStackTrace();
                        }
                        break;

                    case"insertarHorariosClub":
                        recibido = entrada.readUTF();
                        recibido = wp.desCifrar(recibido, "123456789123456789");
                        Horario horarioPadre = g.fromJson(recibido, Horario.class);
                        String strSalidas2 = null;
                        ArrayList<Horario> arrayHorarios2 = new ArrayList<>();

                        try{
                            db.insertarHorarioClub(horarioPadre);

                            ResultSet rs = db.sacarHorariosClub(horarioPadre.getIdClub());
                            while (rs.next() && rs != null) {
                                Horario horario = new Horario();

                                horario.setIdHorario(rs.getInt("idHorario"));
                                horario.setIdClub(rs.getInt("idClub"));
                                horario.setIdEquipo(rs.getInt("idEquipo"));
                                horario.setDiaDeLaSemana(rs.getString("diaDeLaSemana"));
                                horario.setHoraInicio(rs.getString("horaInicio"));
                                horario.setHoraFin(rs.getString("horaFin"));
                                horario.setUbicacion(rs.getString("ubicacion"));
                                horario.setUrlUbicacion(rs.getString("urlUbicacion"));
                                arrayHorarios2.add(horario);
                            }
                            if (arrayHorarios2.size() > -1){
                                strSalidas2 = wp.cifrar(g.toJson(arrayHorarios2), "123456789123456789");
                                System.out.println(wp.desCifrar(strSalidas2, "123456789123456789"));
                                salida.writeUTF(strSalidas2);
                            }else{
                                salida.writeUTF(null);
                            }
                        }catch (SQLException e){
                            e.printStackTrace();
                        }

                        break;

                    case "eliminarHorarioClub":
                        recibido = entrada.readUTF();
                        recibido = wp.desCifrar(recibido, "123456789123456789");
                        Horario horarioClub = g.fromJson(recibido, Horario.class);
                        System.out.println("-------" + horarioClub);
                        String strSalidaClub = null;
                        ArrayList<Horario> arrayHorariosClub = new ArrayList<>();

                        try{
                            db.eliminarHorario(horarioClub);

                            ResultSet rs = db.sacarHorariosClub(horarioClub.getIdClub());
                            while (rs.next() && rs != null) {
                                Horario horario = new Horario();

                                horario.setIdHorario(rs.getInt("idHorario"));
                                horario.setIdClub(rs.getInt("idClub"));
                                horario.setIdEquipo(rs.getInt("idEquipo"));
                                horario.setDiaDeLaSemana(rs.getString("diaDeLaSemana"));
                                horario.setHoraInicio(rs.getString("horaInicio"));
                                horario.setHoraFin(rs.getString("horaFin"));
                                horario.setUbicacion(rs.getString("ubicacion"));
                                horario.setUrlUbicacion(rs.getString("urlUbicacion"));
                                arrayHorariosClub.add(horario);
                            }
                            if (arrayHorariosClub.size() > -1){
                                strSalidaClub = wp.cifrar(g.toJson(arrayHorariosClub), "123456789123456789");
                                System.out.println(wp.desCifrar(strSalidaClub, "123456789123456789"));
                                salida.writeUTF(strSalidaClub);
                            }else{
                                salida.writeUTF(null);
                            }
                        }catch (SQLException e){
                            e.printStackTrace();
                        }
                        break;

                    case "sacarNoticiasClub":
                        recibido = entrada.readUTF();
                        recibido = wp.desCifrar(recibido, "123456789123456789");
                        Club clubNoticia = g.fromJson(recibido, Club.class);
                        ArrayList<Noticia> arrayNoticias = new ArrayList<>();
                        try {
                            ResultSet rs = db.sacarNoticiasClub(clubNoticia.getIdClub());
                            while (rs.next() && rs != null) {
                                Noticia noticia = new Noticia();

                                noticia.setIdNoticia(rs.getInt("idNoticia"));
                                noticia.setIdClub(rs.getInt("idClub"));
                                noticia.setTitulo(rs.getString("titulo"));
                                noticia.setDescripcion(rs.getString("descripcion"));
                                noticia.setFecha(rs.getDate("fecha"));
                                noticia.setTexto(rs.getString("texto"));
                                arrayNoticias.add(noticia);
                            }
                            if (arrayNoticias.size() > -1) {
                                System.out.println(g.toJson(arrayNoticias));
                                salida.writeUTF(wp.cifrar(g.toJson(arrayNoticias), "123456789123456789"));
                            } else {
                                salida.writeUTF(null);
                            }
                        }catch (SQLException e){
                            e.printStackTrace();
                        }
                        break;

                    case "sacarNoticiasJugador":
                        recibido = entrada.readUTF();
                        recibido = wp.desCifrar(recibido, "123456789123456789");
                        Club jugadorNoticia = g.fromJson(recibido, Club.class);
                        ArrayList<Noticia> arrayNoticiasjugador = new ArrayList<>();
                        try {
                            ResultSet rs = db.sacarTodasLasNoticias();
                            while (rs.next() && rs != null) {
                                Noticia noticia = new Noticia();

                                noticia.setIdNoticia(rs.getInt("idNoticia"));
                                noticia.setIdClub(rs.getInt("idClub"));
                                noticia.setTitulo(rs.getString("titulo"));
                                noticia.setDescripcion(rs.getString("descripcion"));
                                noticia.setFecha(rs.getDate("fecha"));
                                noticia.setTexto(rs.getString("texto"));
                                arrayNoticiasjugador.add(noticia);
                            }
                            if (arrayNoticiasjugador.size() > -1) {
                                System.out.println(g.toJson(arrayNoticiasjugador));
                                salida.writeUTF(wp.cifrar(g.toJson(arrayNoticiasjugador), "123456789123456789"));
                            } else {
                                salida.writeUTF(null);
                            }
                        }catch (SQLException e){
                            e.printStackTrace();
                        }
                        break;


                    case "eliminarNoticiaClub":
                        recibido = entrada.readUTF();
                        recibido = wp.desCifrar(recibido, "123456789123456789");
                        Noticia noticiaEliminar = g.fromJson(recibido, Noticia.class);
                        System.out.println(noticiaEliminar);
                        ArrayList<Noticia> arrayNoticiasCompleto = new ArrayList<>();
                        try {
                            db.eliminarNoticiaClub(noticiaEliminar);
                            ResultSet rs = db.sacarNoticiasClub(noticiaEliminar.getIdClub());
                            while (rs.next() && rs != null) {
                                Noticia noticia = new Noticia();

                                noticia.setIdNoticia(rs.getInt("idNoticia"));
                                noticia.setIdClub(rs.getInt("idClub"));
                                noticia.setTitulo(rs.getString("titulo"));
                                noticia.setDescripcion(rs.getString("descripcion"));
                                noticia.setFecha(rs.getDate("fecha"));
                                noticia.setTexto(rs.getString("texto"));
                                arrayNoticiasCompleto.add(noticia);
                            }
                            if (arrayNoticiasCompleto.size() > -1) {

                                salida.writeUTF(wp.cifrar(g.toJson(arrayNoticiasCompleto), "123456789123456789"));
                            } else {
                                salida.writeUTF(null);
                            }
                        }catch (SQLException e){
                            e.printStackTrace();
                        }
                        break;

                    case "actualizarNoticiaClub":
                        recibido = entrada.readUTF();
                        recibido = wp.desCifrar(recibido, "123456789123456789");
                        Noticia noticiaModificar = g.fromJson(recibido, Noticia.class);

                        try{
                            db.modificarNoticiaClub(noticiaModificar);
                        }catch (SQLException e){
                            e.printStackTrace();
                        }
                        break;

                    case "insertarNoticiaClub":
                        recibido = entrada.readUTF();
                        recibido = wp.desCifrar(recibido, "123456789123456789");
                        Noticia noticiaNueva = g.fromJson(recibido, Noticia.class);

                        try{
                            db.nuevaNoticiaClub(noticiaNueva);
                        }catch (SQLException e){
                            e.printStackTrace();
                        }
                        break;
                }


            }


        } catch (IOException | SQLException ex) {
            System.err.println("Cliente desconectado con el id : " + idSessio);
        }
    }
}