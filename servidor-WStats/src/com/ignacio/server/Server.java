package com.ignacio.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Server {
    public static void main(String args[]) throws IOException {
        ServerSocket ss;
        ArrayList<ServidorHilo> listaHilos = new ArrayList<>();
        System.out.print("Inicializando servidor... ");
        try {
            //Se crea el ServerSocket
            ss = new ServerSocket(4444);
            System.out.println("\t[OK]");
            //Se crea un id de sesión
            int idSession = 1;
            //Se crea el bucle para recibir las peticiones de los clientes
            while (true) {
                Socket socket;
                socket = ss.accept(); //Se aceptan las peticiones de los clientes
                System.out.println("Nueva conexión entrante: "+socket);
                /*Se crea un hilo por cada petición para poder atenderla de
                forma independiente, y a la vez. Todo el proceso de comunicación
                se realizará a través de este Thread */

                ServidorHilo sHilo =new ServidorHilo(socket, idSession);
                sHilo.start();
                sHilo.join();

                //((com.ignacio.server.ServidorHilo) new com.ignacio.server.ServidorHilo(socket, idSession)).start();
                idSession++;
            }
        } catch (IOException | InterruptedException ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
