package com.example.wstats.registro;

import java.time.LocalDate;
import java.util.Date;

public class Registro {
    private String nombre;
    private String apellidos;
    private String correo;
    private LocalDate fNacimiento;
    private String ciudad;
    private String password;
    private String hijo;

    public Registro(){

    }

    public Registro(String nombre, String apellidos, String correo, LocalDate fNacimiento, String ciudad, String password, String hijo) {
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.correo = correo;
        this.fNacimiento = fNacimiento;
        this.ciudad = ciudad;
        this.password = password;
        this.hijo = hijo;
    }

    @Override
    public String toString() {
        return "Registro{" +
                "nombre='" + nombre + '\'' +
                ", apellidos='" + apellidos + '\'' +
                ", correo='" + correo + '\'' +
                ", fNacimiento=" + fNacimiento +
                ", ciudad='" + ciudad + '\'' +
                ", password='" + password + '\'' +
                ", hijo='" + hijo + '\'' +
                '}';
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public LocalDate getfNacimiento() {
        return fNacimiento;
    }

    public void setfNacimiento(LocalDate fNacimiento) {
        this.fNacimiento = fNacimiento;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getHijo() {
        return hijo;
    }

    public void setHijo(String hijo) {
        this.hijo = hijo;
    }
}
