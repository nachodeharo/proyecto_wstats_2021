package com.example.wstats.principal.ui.plantilla.jugadores;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;

import com.example.wstats.R;
import com.example.wstats.claseGlobal.Variables;
import com.example.wstats.registro.DatePickerFragment;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.ignacio.Club;
import com.ignacio.Entrenador;
import com.ignacio.Equipo;
import com.ignacio.Jugador;
import com.ignacio.Usuario;
import com.nacho.WHashCrypt;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.Socket;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class Modificar_jugadorActivity extends AppCompatActivity implements View.OnClickListener{

    EditText nombre, apellido, dni, email, fecha, tarifa;
    Jugador jugador;
    String[] arrayPosicion;
    ArrayList<Entrenador> arrayEntrenadores;
    ArrayList<Equipo> arrayEquipos;
    String[] arrayStrEntrenadores;
    String[] arrayStsEquipos;

    ArrayAdapter<String> adapterEquipo;
    ArrayAdapter<String> adapterEntrenador;

    Spinner spinnerPosicion, spinnerEquipo, spinnerEntrenadores;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jugadordetalle);
        Intent intent=getIntent();
        Gson g = new Gson();
        //SPINNER DE POSICION
        spinnerPosicion = (Spinner) findViewById(R.id.spinner_jugadorPosicion);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.posiciones_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerPosicion.setAdapter(adapter);

        arrayEntrenadores = new ArrayList<>();
        arrayEquipos = new ArrayList<>();

        arrayPosicion = getResources().getStringArray(R.array.posiciones_array);
        //Accedo a los editText
        nombre = findViewById(R.id.jugador_nombre);
        apellido = findViewById(R.id.jugador_apellido);
        dni = findViewById(R.id.jugador_dni);
        email = findViewById(R.id.jugador_correo);
        fecha = findViewById(R.id.jugador_fecha);
        fecha.setOnClickListener(this);
        tarifa = findViewById(R.id.jugador_tarifa);

        //Empiezo a establecer datos
        jugador = new Jugador();
        jugador = g.fromJson(intent.getStringExtra("jugador"), Jugador.class);

        nombre.setText(jugador.getNombre());
        apellido.setText(jugador.getApellidos());
        dni.setText(jugador.getDni());
        email.setText(jugador.getCorreo());
        fecha.setText(jugador.getFechanacimiento().toString());
        tarifa.setText(jugador.getTarifa());
        //Bucle para detectar que posicion tiene (Recordamos que la guardamos en la bbdd y en android
        //No existe el exacto "ComboBox con sus facilidades")
        for (int i = 0 ; i< arrayPosicion.length ; i++){
            if (arrayPosicion[i].equals(jugador.getPosicion())){
                spinnerPosicion.setSelection(i);
                break;
            }
        }
        findViewById(R.id.imgGurardarJugador).setOnClickListener(this);
        //Pedir datos
        sacarDatosServidor();

        //Establezco datos de los arrays
        arrayStsEquipos = new String[arrayEquipos.size()];
        arrayStrEntrenadores = new String[arrayEntrenadores.size()];

        for (int i = 0 ; i< arrayEquipos.size() ; i++){
            System.out.println(arrayEquipos.get(i));
            arrayStsEquipos[i] = arrayEquipos.get(i).getNombreEquipo();
        }
        Entrenador entrenador;
        for (int i = 0 ; i< arrayEntrenadores.size() ; i++){
            entrenador = new Entrenador();
            entrenador = arrayEntrenadores.get(i);
            System.out.println(entrenador);
            arrayStrEntrenadores[i] = entrenador.getNombre() + " " + entrenador.getApellidos();
        }

        System.out.println("---");
        for(String str : arrayStrEntrenadores){
            System.out.println(str);
        }

        //Lo paso al spinner
        //SPINNER DE EQUIPO

        spinnerEquipo = (Spinner) findViewById(R.id.spinner_jugadorEquipo);
        adapterEquipo = new ArrayAdapter<String>
        (this, android.R.layout.simple_spinner_item,
                arrayStsEquipos);
        adapterEquipo.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerEquipo.setAdapter(adapterEquipo);


        //SPINNER DE ENTRENADORES
        spinnerEntrenadores = (Spinner) findViewById(R.id.spinner_jugadorEntrenador);
        adapterEntrenador = new ArrayAdapter<String>
                (this, android.R.layout.simple_spinner_item,
                        arrayStrEntrenadores);
        adapterEntrenador.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerEntrenadores.setAdapter(adapterEntrenador);


        //SELECCIONAMOS EL SPINNER CORRECTO CON LOS VALORES

        for (int i = 0 ; i < arrayEquipos.size() ; i++){
            if (jugador.getIdEquipo() == arrayEquipos.get(i).getIdEquipo()){
                spinnerEquipo.setSelection(i);
                break;
            }
        }

        for (int i = 0 ; i < arrayEntrenadores.size() ; i++){
            if (jugador.getIdEntrenador() == arrayEntrenadores.get(i).getIdEntrenador()){
                spinnerEntrenadores.setSelection(i);
                break;
            }
        }
        // https://developer.android.com/guide/topics/ui/controls/spinner
    }

    private void sacarDatosServidor() {
        try {
            WHashCrypt wp = new WHashCrypt();
            Gson g = new Gson();
            System.out.println("ENTRO A PROBAR");
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
            Socket socket = new Socket(Variables.IP_SERVER, Variables.PUERTO_SERVER);
            // Declaramos un DataInputStream para leer los datos
            DataInputStream entrada = new DataInputStream(socket.getInputStream());

            // Declaramos un DataOutputStream para escribir los datos
            DataOutputStream salida = new DataOutputStream(socket.getOutputStream());
            String strEnvio = null;
            Usuario user = new Usuario();
            user.setTipo("sacarEquipoEntrenadorClub");

            strEnvio = wp.cifrar(g.toJson(user), "123456789123456789");
            salida.writeUTF(strEnvio);

            //Pido los entrenadores y equipos del club
            strEnvio = wp.cifrar(g.toJson(Variables.club), "123456789123456789");
            salida.writeUTF(strEnvio);

            System.out.println("PASO LOS ARRAYS:");

            String recibido = entrada.readUTF();
            recibido = wp.desCifrar(recibido, "123456789123456789");
            System.out.println(recibido);
            JsonArray arry2 = new JsonParser().parse(recibido).getAsJsonArray();
            arrayEquipos.clear();
            for (JsonElement jsonElement : arry2) {
                arrayEquipos.add(g.fromJson(jsonElement, Equipo.class));
            }

            recibido = entrada.readUTF();
            recibido = wp.desCifrar(recibido, "123456789123456789");
            System.out.println(recibido);
            JsonArray arry = new JsonParser().parse(recibido).getAsJsonArray();
            arrayEntrenadores.clear();
            for (JsonElement jsonElement : arry) {
                arrayEntrenadores.add(g.fromJson(jsonElement, Entrenador.class));
            }


            //adapter.notifyDataSetChanged();

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void actualizarDatosServidor(){
        try {
            WHashCrypt wp = new WHashCrypt();
            Gson g = new Gson();
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
            Socket socket = new Socket(Variables.IP_SERVER, Variables.PUERTO_SERVER);
            // Declaramos un DataInputStream para leer los datos
            DataInputStream entrada = new DataInputStream(socket.getInputStream());

            // Declaramos un DataOutputStream para escribir los datos
            DataOutputStream salida = new DataOutputStream(socket.getOutputStream());
            String strEnvio = null;
            Usuario user = new Usuario();
            user.setTipo("actualizarJugador");
            System.out.println("Actualizamos");

            strEnvio = wp.cifrar(g.toJson(user), "123456789123456789");
            salida.writeUTF(strEnvio);

            jugador.setNombre(nombre.getText().toString());
            jugador.setApellidos(apellido.getText().toString());
            jugador.setDni(dni.getText().toString());
            jugador.setCorreo(email.getText().toString());
            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
            jugador.setFechanacimiento(LocalDate.parse(fecha.getText().toString(), dateFormatter));
            jugador.setTarifa(tarifa.getText().toString());
            //Meto los combos
            String str = (String) spinnerEntrenadores.getSelectedItem();
            String nombreCompleto;
            // bucle entrenadores
            for (int i = 0 ; i < arrayEntrenadores.size() ; i++){
                nombreCompleto = arrayEntrenadores.get(i).getNombre() + " " + arrayEntrenadores.get(i).getApellidos();
                 if (str.equalsIgnoreCase(nombreCompleto)){
                    jugador.setIdEntrenador(arrayEntrenadores.get(i).getIdEntrenador());
                }
            }

            jugador.setPosicion(spinnerPosicion.getSelectedItem() + "");

            //bucle equipo
            str = (String) spinnerEquipo.getSelectedItem();
            for (int i = 0 ; i < arrayEquipos.size() ; i++){
                if (str.equalsIgnoreCase(arrayEquipos.get(i).getNombreEquipo())){
                    jugador.setIdEquipo(arrayEquipos.get(i).getIdEquipo());
                }
            }
            //Envio el jugador editado
            strEnvio = wp.cifrar(g.toJson(jugador), "123456789123456789");
            salida.writeUTF(strEnvio);


            //adapter.notifyDataSetChanged();

        }catch (Exception e){
            e.printStackTrace();
        }
    }
    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.imgGurardarJugador:
                new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
                        .setTitleText("¿Estas seguro?")
                        .setContentText("¡Todos tus datos serán actualizados!")
                        .setConfirmText("Actualizar")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                // reuse previous dialog instance
                                actualizarDatosServidor();
                                sDialog.setTitleText("¡Actualizado!")
                                        .setContentText("¡Tus datos se han modificado correctamente!")
                                        .setConfirmText("OK")
                                        .setConfirmClickListener(null)
                                        .changeAlertType(SweetAlertDialog.SUCCESS_TYPE);

                                cambiarActivity();
                            }
                        })
                        .show();
                break;

            case R.id.jugador_fecha:
                showDatePickerDialog();
                break;
        }

    }

    private void cambiarActivity() {
        Intent i = new Intent(this, club_jugadoresActivity.class);
        startActivity(i);
    }

    private void showDatePickerDialog() {
        DatePickerFragment newFragment = DatePickerFragment.newInstance(new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                // +1 because January is zero
                if ((month+1) < 10){
                    if (day < 10){
                        final String selectedDate = "0" + day + "/0" + (month+1) + "/" + year;
                        fecha.setText(selectedDate);
                    }else{
                        final String selectedDate = day + "/0" + (month+1) + "/" + year;
                        fecha.setText(selectedDate);
                    }

                }else {
                    final String selectedDate = day + "/" + (month + 1) + "/" + year;
                    fecha.setText(selectedDate);
                }
            }
        });

        newFragment.show(getSupportFragmentManager(), "datePicker");
    }
}