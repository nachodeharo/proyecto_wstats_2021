package com.example.wstats.principal.ui.ajustes;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.example.wstats.R;
import com.example.wstats.claseGlobal.Util;
import com.example.wstats.claseGlobal.Variables;
import com.google.gson.Gson;
import com.ignacio.Club;
import com.ignacio.Usuario;
import com.nacho.WHashCrypt;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.nio.ByteBuffer;

import cn.pedant.SweetAlert.SweetAlertDialog;



public class AjustesFragment extends Fragment implements View.OnClickListener {

    private AjustesViewModel homeViewModel;
    private EditText tv_name;
    private EditText tv_adress;
    private EditText club_correo;
    private EditText club_telefono;
    private EditText club_direccion;
    private EditText club_password;
    private ImageView club_imagen;
    private View root;
    private Bitmap fotoBitmap;
    private static final int IMAGE_PICK_CODE = 1000;
    private static final int PEMISSION_CODE = 1001;
    private static final int RESULT_OK = 1000;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel =
                new ViewModelProvider(this).get(AjustesViewModel.class);
        root = inflater.inflate(R.layout.fragment_ajustesclub, container, false);

        tv_name = root.findViewById(R.id.jugador_nombre);
        tv_name.setText(Variables.club.getNombre());

        tv_adress = root.findViewById(R.id.jugador_apellido);
        tv_adress.setText(Variables.club.getCiudad());

        club_correo = root.findViewById(R.id.jugador_correo);
        club_correo.setText(Variables.club.getCorreo());

        club_telefono = root.findViewById(R.id.jugador_fecha);
        club_telefono.setText(String.valueOf(Variables.club.getTelefono()));

        club_direccion = root.findViewById(R.id.club_ciudad);
        club_direccion.setText(Variables.club.getDireccion());

        club_password = root.findViewById(R.id.club_password);
        club_password.setOnClickListener(this);
        //club_password.setText(Variables.club.getPass());
        club_imagen = root.findViewById(R.id.img_jugador);
        club_imagen.setOnClickListener(this);
        club_imagen.setImageBitmap(Util.getBitmap(Variables.club.getFoto()));


        ImageView imagen = root.findViewById(R.id.imgGurardarJugador);
        imagen.setOnClickListener(this);
        return root;
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.imgGurardarJugador:
                new SweetAlertDialog(getContext(), SweetAlertDialog.WARNING_TYPE)
                        .setTitleText("¿Estas seguro?")
                        .setContentText("¡Todos tus datos serán actualizados!")
                        .setConfirmText("Actualizar")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                // reuse previous dialog instance
                                actualizarDatos();
                                conectarServidor();
                                sDialog.setTitleText("¡Actualizado!")
                                        .setContentText("¡Tus datos se han modificado correctamente!")
                                        .setConfirmText("OK")
                                        .setConfirmClickListener(null)
                                        .changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
                            }
                        })
                        .show();
                break;
            case R.id.img_jugador:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                    if (getActivity().checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                            ==  PackageManager.PERMISSION_DENIED){
                        String[] permissions = {Manifest.permission.READ_EXTERNAL_STORAGE};
                        // Mostramos el permiso
                        requestPermissions(permissions, PEMISSION_CODE);
                    }else{
                        pickImageFromGallery();
                    }
                }else{
                    pickImageFromGallery();
                }
                break;
            case R.id.club_password:
                System.out.println("ENTRÓ");
                LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                final View layout = inflater.inflate(R.layout.confirm_pass, (ViewGroup) root.findViewById(R.id.root));
                final EditText password1 = (EditText) layout.findViewById(R.id.txtNuevaCat);
                final EditText password2 = (EditText) layout.findViewById(R.id.EditText_Pwd2);
                final TextView error = (TextView) layout.findViewById(R.id.TextView_PwdProblem);


                password2.addTextChangedListener(new TextWatcher() {
                    public void afterTextChanged(Editable s) {
                        String strPass1 = password1.getText().toString();
                        String strPass2 = password2.getText().toString();

                        if (password1.length() < 5){
                            password1.setError("Debe tener >5 carácteres");
                        }
                        if (password2.length() < 5){
                            password2.setError("Debe tener >5 carácteres");
                        }

                        if (strPass1.equals(strPass2)) {
                            error.setText("Coinciden");
                        } else {
                            error.setText("No coinciden");
                        }

                    }

                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
                    public void onTextChanged(CharSequence s, int start, int before, int count) {}
                });

                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setView(layout);
                builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                    }
                });

                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        String strPassword1 = password1.getText().toString();
                        String strPassword2 = password2.getText().toString();
                        if (strPassword1.equals(strPassword2)) {
                            if(strPassword1.length() > 5){
                                // Guardo contra y la subo a bbdd
                                WHashCrypt wp = new WHashCrypt();
                                Variables.club.setPass(wp.hashear(strPassword1));
                                conectarServidor();
                                new SweetAlertDialog(getContext(), SweetAlertDialog.SUCCESS_TYPE)
                                        .setTitleText("¡Enhorabuena!")
                                        .setContentText("¡Contraseña cambiada!")
                                        .show();
                                dialog.dismiss();
                            }else{
                                new SweetAlertDialog(getContext(), SweetAlertDialog.ERROR_TYPE)
                                        .setTitleText("Contraseña inváida")
                                        .setContentText("Debe tener >5 carácteres")
                                        .show();
                            }
                        }else{
                            new SweetAlertDialog(getContext(), SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("Contraseña inváida")
                                    .setContentText("Deben coincidir")
                                    .show();
                        }
                    }
                });

                AlertDialog alert = builder.create();
                alert.show();

                break;
        }
    }

    private void pickImageFromGallery() {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        intent.setType("image/*");
        startActivityForResult(intent, IMAGE_PICK_CODE);
    }

    private void conectarServidor() {
        try {
            WHashCrypt wp = new WHashCrypt();
            Gson g = new Gson();
            System.out.println("ENTRO A PROBAR");
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
            Socket socket = new Socket("192.168.56.1", 4444);
            System.out.println("SERVER CONECTADO");
            // Declaramos un DataInputStream para leer los datos
            DataInputStream entrada = new DataInputStream(socket.getInputStream());

            // Declaramos un DataOutputStream para escribir los datos
            DataOutputStream salida = new DataOutputStream(socket.getOutputStream());
            String strEnvio = null;
            Usuario user = new Usuario();
            user.setTipo("actualizarClubs");

            strEnvio = wp.cifrar(g.toJson(user), "123456789123456789");
            salida.writeUTF(strEnvio);

            Club club = new Club();
            actualizarDatos();
            Variables.club.setFoto(new byte[0]);
            club = Variables.club;

            strEnvio = g.toJson(club);
            strEnvio = wp.cifrar(strEnvio, "123456789123456789");

            System.out.println(club);
            salida.writeUTF(strEnvio);

            /*
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            ImageIO.write(club_imagen, "png", byteArrayOutputStream);

            byte[] size = ByteBuffer.allocate(4).putInt(byteArrayOutputStream.size()).array();
            System.out.println(byteArrayOutputStream.size());
            outputStream.write(byteArrayOutputStream.toByteArray());
             */

            salida.write(Util.getBytes(fotoBitmap));

            String recibido = entrada.readUTF();
            if (recibido.equals("OK")){
                System.out.println("CAMBIADO");
            }
            entrada.close();
            salida.close();
        }catch (Exception e){
            e.printStackTrace();
        }

    }
    void actualizarDatos(){
        try{
            Variables.club.setNombre(tv_name.getText().toString());
            Variables.club.setCiudad(tv_adress.getText().toString());
            //Variables.club.setCorreo(club_correo.getText().toString());
            //Variables.club.setPass(club_password.getText().toString());
            Variables.club.setTelefono(Integer.valueOf(club_telefono.getText().toString()));
            //Variables.club.setFoto((Blob) fotoBitmap);
            Variables.club.setDireccion(club_direccion.getText().toString());
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){
            case PEMISSION_CODE:{
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    //PERMISOS CORRECTOS
                    pickImageFromGallery();
                }else{
                    //PERMISOS INSUFICIENTES
                    Toast.makeText(getContext(), "Permiso denegado", Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == IMAGE_PICK_CODE){
            //ponemos imagen
            System.out.println(data.getData());
            try {
                fotoBitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), data.getData());
            } catch (IOException e) {
                e.printStackTrace();
            }
            //club_imagen.setImageBitmap(fotoBitmap);
            System.out.println(fotoBitmap);
            club_imagen.setImageBitmap(fotoBitmap);
        }else{
            System.out.println("ERROR");
        }
    }

}