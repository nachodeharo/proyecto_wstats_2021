package com.example.wstats.principal.ui.plantilla.equipos;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.wstats.R;
import com.google.gson.internal.LinkedTreeMap;
import com.ignacio.Equipo;

import java.util.ArrayList;

public class EquipoAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<Equipo> listaEquipos;
    private LayoutInflater inflater;

    /*
    Constructor de la clase, se le pasa la activity y un arraylist
     */
    public EquipoAdapter(Activity context, ArrayList<Equipo> listaEquipos){
        this.context = context;
        this.listaEquipos = listaEquipos;
        this.inflater = LayoutInflater.from(context);
    }

    /*
    Lo que va a llevar el holder
     */
    public static class ViewHolder{
        TextView nombre;
    }
    /*
    Getter del tamaño de la lista
     */
    @Override
    public int getCount() {
        return this.listaEquipos.size();
    }

    /*
    getter de la posicion de la lista
     */
    @Override
    public Object getItem(int position) {
        return this.listaEquipos.get(position);
    }

    /*
    Getter de la id de la lista
     */
    @Override
    public long getItemId(int position) {
        return position;
    }

    /*
    Permite insertar en la lista cada producto
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView ==null){
            convertView = inflater.inflate(R.layout.listacategoria, null);
            holder = new ViewHolder();
            holder.nombre = (TextView) convertView.findViewById(R.id.txtNombreCategoria);
            convertView.setTag(holder);
        }
        else{
            holder =(ViewHolder) convertView.getTag();
        }
        //System.out.println("ENTRAMOS!");
        /*
        Object getrow = this.listaEquipos.get(position);

        LinkedTreeMap<Object,Object> t = (LinkedTreeMap) getrow;

        //String name = t.get("nombreEquipos").toString();
        holder.nombre.setText(t.get("nombreEquipo").toString());
         */

        Equipo equipo = listaEquipos.get(position);
        holder.nombre.setText(equipo.getNombreEquipo());

        return convertView;
    }
}

