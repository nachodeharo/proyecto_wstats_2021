package com.example.wstats.claseGlobal;

import com.ignacio.Aficionado;
import com.ignacio.Club;
import com.ignacio.Entrenador;
import com.ignacio.Jugador;

public class Variables {
    public static Club club = new Club();
    public static Entrenador entrenador = new Entrenador();
    public static Jugador jugador = new Jugador();
    public static Aficionado aficionado = new Aficionado();
    public static final String IP_SERVER = "192.168.56.1";
    public static final int PUERTO_SERVER = 4444;
    private static final int IMAGE_PICK_CODE = 1000;
    private static final int PEMISSION_CODE = 1001;


}
