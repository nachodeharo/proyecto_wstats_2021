package com.example.wstats.login.ui;

import android.app.Activity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.wstats.R;
import com.example.wstats.principal.ClubActivity;
import com.example.wstats.registro.RegisterActivity;
import com.example.wstats.registro.javaAyudas.TipoProductoAdapter;
import com.example.wstats.registro.javaAyudas.TiposProducto;

import java.util.ArrayList;

public class LoginActivity extends AppCompatActivity implements  View.OnClickListener, AdapterView.OnItemClickListener, AdapterView.OnItemSelectedListener {

    private LoginViewModel loginViewModel;
    TextView textoRegistro;
    private TipoProductoAdapter adapter;
    private ArrayList<TiposProducto> listaProductos;
    private TiposProducto tipoProducto;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        loginViewModel = new ViewModelProvider(this, new LoginViewModelFactory())
                .get(LoginViewModel.class);

        final EditText usernameEditText = findViewById(R.id.username);
        final EditText passwordEditText = findViewById(R.id.password);
        final Button loginButton = findViewById(R.id.login);
        final ProgressBar loadingProgressBar = findViewById(R.id.loading);

        textoRegistro = findViewById(R.id.text_click);

        listaProductos = new ArrayList<TiposProducto>();
        ImageView imagen = null;
        Bitmap miBitMap= null;

        /*
        Creo el spinner para el desplegable
         */
        TiposProducto producto = new TiposProducto();
        TiposProducto producto2 = new TiposProducto();
        TiposProducto producto3 = new TiposProducto();
        TiposProducto producto4 = new TiposProducto();

        producto.setNombreProducto("Jugador");
        miBitMap= BitmapFactory.decodeResource(getResources(), R.drawable.jugador);
        producto.setFoto(miBitMap);
        listaProductos.add(producto);

        producto2.setNombreProducto("Entrenador");
        miBitMap= BitmapFactory.decodeResource(getResources(), R.drawable.entrenador);
        producto2.setFoto(miBitMap);
        listaProductos.add(producto2);

        producto3.setNombreProducto("Aficionado");
        miBitMap= BitmapFactory.decodeResource(getResources(), R.drawable.aficionado);
        producto3.setFoto(miBitMap);
        listaProductos.add(producto3);

        producto4.setNombreProducto("Club");
        miBitMap= BitmapFactory.decodeResource(getResources(), R.drawable.ball);
        producto4.setFoto(miBitMap);
        listaProductos.add(producto4);

         /*
        Los añado al spinner
         */
        Spinner lvLista = findViewById(R.id.spinner);
        adapter = new TipoProductoAdapter(this, listaProductos);
        lvLista.setAdapter(adapter);
        lvLista.setOnItemSelectedListener(this);


        textoRegistro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                crearActivity();
            }
        });

        loginViewModel.getLoginFormState().observe(this, new Observer<LoginFormState>() {
            @Override
            public void onChanged(@Nullable LoginFormState loginFormState) {
                if (loginFormState == null) {
                    return;
                }
                loginButton.setEnabled(loginFormState.isDataValid());
                if (loginFormState.getUsernameError() != null) {
                    usernameEditText.setError(getString(loginFormState.getUsernameError()));
                }
                if (loginFormState.getPasswordError() != null) {
                    passwordEditText.setError(getString(loginFormState.getPasswordError()));
                }
            }
        });

        loginViewModel.getLoginResult().observe(this, new Observer<LoginResult>() {
            @Override
            public void onChanged(@Nullable LoginResult loginResult) {
                if (loginResult == null) {
                    return;
                }
                // Permite ver la progressBar
                //loadingProgressBar.setVisibility(View.GONE);
                if (loginResult.getError() != null) {
                    showLoginFailed(loginResult.getError());
                }
                if (loginResult.getSuccess() != null) {
                    updateUiWithUser(loginResult.getSuccess());
                }
                setResult(Activity.RESULT_OK);

                //DESTRUYE LA ACTIVITY UNA VEZ SE LOGEE
                //finish();
            }
        });

        TextWatcher afterTextChangedListener = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // ignore
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // ignore
            }

            @Override
            public void afterTextChanged(Editable s) {
                loginViewModel.loginDataChanged(usernameEditText.getText().toString(),
                        passwordEditText.getText().toString());
            }
        };
        usernameEditText.addTextChangedListener(afterTextChangedListener);
        passwordEditText.addTextChangedListener(afterTextChangedListener);
        passwordEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    loginViewModel.login(usernameEditText.getText().toString(),
                            passwordEditText.getText().toString());
                }
                return false;
            }
        });

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //loadingProgressBar.setVisibility(View.VISIBLE);
                loginViewModel.login(usernameEditText.getText().toString(),
                        passwordEditText.getText().toString());
            }
        });
    }

    private void crearActivity() {
        Intent intent = new Intent(this, RegisterActivity.class);
        startActivity(intent);
    }

    private void updateUiWithUser(LoggedInUserView model) {
        String welcome = getString(R.string.welcome) + model.getDisplayName();
        // TODO : initiate successful logged in experience
        Toast.makeText(getApplicationContext(), welcome, Toast.LENGTH_LONG).show();
        Intent intent = new Intent(this, ClubActivity.class);
        startActivity(intent);
    }

    private void showLoginFailed(@StringRes Integer errorString) {
        Toast.makeText(getApplicationContext(), errorString, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}