package com.example.wstats.principal.ui.plantilla.equipos;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.example.wstats.R;
import com.example.wstats.claseGlobal.Variables;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.ignacio.Club;
import com.ignacio.Equipo;
import com.ignacio.Usuario;
import com.nacho.WHashCrypt;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.Socket;
import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class club_categoriasActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemLongClickListener, AdapterView.OnItemClickListener {

    private ArrayList<Equipo> arrayList;
    private EquipoAdapter adapter;
    private Equipo categoriaSeleccionada;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_club_equipos);
        arrayList = new ArrayList<Equipo>();
        Button btn = findViewById(R.id.btnNuevoJugador);
        btn.setOnClickListener(this);

        ListView listView = (ListView) findViewById(R.id.listaClubJugadores);
        sacarDatosServidor();

        adapter = new EquipoAdapter(this, arrayList);
        listView.setAdapter(adapter);
        listView.setOnItemLongClickListener(this);
        registerForContextMenu(listView);
        System.out.println("yepee");

        TextView mombreClub = findViewById(R.id.txtNombreCategorias);
        mombreClub.setText(Variables.club.getNombre());
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        setContentView(R.layout.activity_club_equipos);
        arrayList = new ArrayList<Equipo>();
        Button btn = findViewById(R.id.btnNuevoJugador);
        btn.setOnClickListener(this);

        ListView listView = (ListView) findViewById(R.id.listaClubJugadores);
        sacarDatosServidor();

        adapter = new EquipoAdapter(this, arrayList);
        listView.setAdapter(adapter);
        listView.setOnItemLongClickListener(this);
        registerForContextMenu(listView);
        System.out.println("yepee");

        TextView mombreClub = findViewById(R.id.txtNombreCategorias);
        mombreClub.setText(Variables.club.getNombre());
    }

    private void sacarDatosServidor() {
        try {
            WHashCrypt wp = new WHashCrypt();
            Gson g = new Gson();
            System.out.println("ENTRO A PROBAR");
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
            Socket socket = new Socket(Variables.IP_SERVER, Variables.PUERTO_SERVER);
            System.out.println("SERVER CONECTADO");
            // Declaramos un DataInputStream para leer los datos
            DataInputStream entrada = new DataInputStream(socket.getInputStream());

            // Declaramos un DataOutputStream para escribir los datos
            DataOutputStream salida = new DataOutputStream(socket.getOutputStream());
            String strEnvio = null;
            Usuario user = new Usuario();
            user.setTipo("sacarEquiposClub");

            strEnvio = wp.cifrar(g.toJson(user), "123456789123456789");
            salida.writeUTF(strEnvio);

            //envio el club para pillar la id
            Club club = Variables.club;
            club.setFoto(new byte[0]);
            strEnvio = wp.cifrar(g.toJson(club), "123456789123456789");
            salida.writeUTF(strEnvio);

            // recibo el array
            String recibido = entrada.readUTF();
            recibido = wp.desCifrar(recibido, "123456789123456789");

            JsonArray arry = new JsonParser().parse(recibido).getAsJsonArray();
            arrayList.clear();
            for (JsonElement jsonElement : arry) {
                arrayList.add(g.fromJson(jsonElement, Equipo.class));
            }
            //adapter.notifyDataSetChanged();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public boolean onContextItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()){

            case R.id.menu_contextual_eliminar:
                new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
                        .setTitleText("¿Estas seguro?")
                        .setContentText("¡Se eliminará este equipo!")
                        .setConfirmText("Eliminar")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                // reuse previous dialog instance
                                arrayList.remove(categoriaSeleccionada);
                                eliminarDatosServidor();
                                sDialog.setTitleText("¡Actualizado!")
                                        .setContentText("¡Tus datos se han modificado correctamente!")
                                        .setConfirmText("OK")
                                        .setConfirmClickListener(null)
                                        .changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
                            }
                        })
                        .show();
                break;
        }

        return super.onContextItemSelected(item);
    }

    private void eliminarDatosServidor() {
        try {
            WHashCrypt wp = new WHashCrypt();
            Gson g = new Gson();
            System.out.println("ENTRO A PROBAR");
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
            Socket socket = new Socket(Variables.IP_SERVER, Variables.PUERTO_SERVER);
            System.out.println("SERVER CONECTADOssss");
            // Declaramos un DataInputStream para leer los datos
            DataInputStream entrada = new DataInputStream(socket.getInputStream());

            // Declaramos un DataOutputStream para escribir los datos
            DataOutputStream salida = new DataOutputStream(socket.getOutputStream());
            String strEnvio = null;
            Usuario user = new Usuario();
            user.setTipo("eliminarEquiposClub");

            strEnvio = wp.cifrar(g.toJson(user), "123456789123456789");
            salida.writeUTF(strEnvio);

            //envio el club para pillar la id
            strEnvio = wp.cifrar(g.toJson(Variables.club), "123456789123456789");
            salida.writeUTF(strEnvio);

            //envio el arraylist para pillar
            salida.writeUTF(categoriaSeleccionada.getIdEquipo()+"");

            // recibo el array actualizado
            String recibido = entrada.readUTF();
            recibido = wp.desCifrar(recibido, "123456789123456789");
            //arrayList = g.fromJson(recibido, ArrayList.class);
            System.out.println("yeyey");
            arrayList.clear();
            JsonArray arry = new JsonParser().parse(recibido).getAsJsonArray();
            for (JsonElement jsonElement : arry) {
                arrayList.add(g.fromJson(jsonElement, Equipo.class));
            }
            adapter.notifyDataSetChanged();

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void actualizarDatosServidor(String s) {
        try {
            WHashCrypt wp = new WHashCrypt();
            Gson g = new Gson();
            System.out.println("ENTRO A PROBAR");
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
            Socket socket = new Socket(Variables.IP_SERVER, Variables.PUERTO_SERVER);
            System.out.println("SERVER CONECTADOssss");
            // Declaramos un DataInputStream para leer los datos
            DataInputStream entrada = new DataInputStream(socket.getInputStream());

            // Declaramos un DataOutputStream para escribir los datos
            DataOutputStream salida = new DataOutputStream(socket.getOutputStream());
            String strEnvio = null;
            Usuario user = new Usuario();
            user.setTipo("meterEquiposClub");

            strEnvio = wp.cifrar(g.toJson(user), "123456789123456789");
            salida.writeUTF(strEnvio);

            //envio el club para pillar la id
            strEnvio = wp.cifrar(g.toJson(Variables.club), "123456789123456789");
            salida.writeUTF(strEnvio);

            //envio el arraylist para pillar
            strEnvio = s;
            salida.writeUTF(strEnvio);

            // recibo el array actualizado
            String recibido = entrada.readUTF();
            recibido = wp.desCifrar(recibido, "123456789123456789");
            //arrayList = g.fromJson(recibido, ArrayList.class);
            System.out.println("yeyey");
            arrayList.clear();
            JsonArray arry = new JsonParser().parse(recibido).getAsJsonArray();
            for (JsonElement jsonElement : arry) {
                arrayList.add(g.fromJson(jsonElement, Equipo.class));
            }
            adapter.notifyDataSetChanged();

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        getMenuInflater().inflate(R.menu.menu_contextual_lista, menu);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){

            case R.id.btnNuevoJugador:
                crearMensaje();
                break;
        }
    }

    private void crearMensaje() {
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        final View layout = inflater.inflate(R.layout.new_equipo, (ViewGroup) findViewById(R.id.root));


        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(layout);
        builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.dismiss();
            }
        });

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                TextView categoria = layout.findViewById(R.id.txtNuevaCat);

                actualizarDatosServidor(categoria.getText().toString());
                new SweetAlertDialog(club_categoriasActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                        .setTitleText("¡Enhorabuena!")
                        .setContentText("¡Has añadido un equipo!")
                        .show();
                dialog.dismiss();

            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        //categoriaSeleccionada = arrayList.get(position);
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        System.out.println(arrayList);
        Object getrow = arrayList.get(position);
        categoriaSeleccionada = (Equipo) getrow;

        //categoriaSeleccionada = arrayList.get(position);
        return false;
    }
}