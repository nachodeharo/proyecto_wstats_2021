package com.example.wstats.principal.ui.plantilla;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.example.wstats.R;
import com.example.wstats.principal.ui.plantilla.equipos.club_categoriasActivity;
import com.example.wstats.principal.ui.plantilla.jugadores.club_jugadoresActivity;


public class HomeFragment extends Fragment implements View.OnClickListener{

    private HomeViewModel homeViewModel;
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel =
                new ViewModelProvider(this).get(HomeViewModel.class);
        View root = inflater.inflate(R.layout.fragment_plantillaclub, container, false);
        //final TextView textView = root.findViewById(R.id.club_text_nombre);
        root.findViewById(R.id.imgClubEntrenador).setOnClickListener(this);
        root.findViewById(R.id.imgClubEquipos).setOnClickListener(this);
        root.findViewById(R.id.imgClubJugadores).setOnClickListener(this);
        return root;
    }


    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()){

            case R.id.imgClubEntrenador:
                break;

            case R.id.imgClubEquipos:
                intent = new Intent(getActivity(), club_categoriasActivity.class);
                startActivity(intent);
                break;

            case R.id.imgClubJugadores:
                intent = new Intent(getActivity(), club_jugadoresActivity.class);
                startActivity(intent);
                break;
        }

    }
}