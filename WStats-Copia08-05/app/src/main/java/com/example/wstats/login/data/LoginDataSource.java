package com.example.wstats.login.data;

import android.os.StrictMode;

import com.example.wstats.claseGlobal.Variables;
import com.example.wstats.login.data.model.LoggedInUser;
import com.google.gson.Gson;
import com.ignacio.Aficionado;
import com.ignacio.Club;
import com.ignacio.Entrenador;
import com.ignacio.Jugador;
import com.ignacio.Usuario;
import com.nacho.WHashCrypt;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Scanner;

/**
 * Class that handles authentication w/ login credentials and retrieves user information.
 */
public class LoginDataSource {
    private int puerto;
    private String host;
    private Socket socket;

    public Result<LoggedInUser> login(String username, String password) {

        try {
            // TODO: Comprobar la autenticidad el logeo
            LoggedInUser fakeUser = null;

            if (conexionCliente(username,password)){
                 fakeUser =
                        new LoggedInUser(
                                java.util.UUID.randomUUID().toString(),
                                username);
                return new Result.Success<>(fakeUser);
            }else {
                return new Result.Error(null);
            }
        } catch (Exception e) {
            return new Result.Error(new IOException("Error en el loggeo", e));
        }
    }

    public void logout() {
        // TODO: revoke authentication
    }

    public static boolean conexionCliente(String username, String password){
        try {
            WHashCrypt wp = new WHashCrypt();
            Gson g = new Gson();
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
            System.out.println("ENTRO A PROBAR");
            Socket socket = new Socket("192.168.56.1", 4444);
            System.out.println("SERVER CONECTADO");
            // Declaramos un DataInputStream para leer los datos
            DataInputStream entrada = new DataInputStream(socket.getInputStream());

            // Declaramos un DataOutputStream para escribir los datos
            DataOutputStream salida = new DataOutputStream(socket.getOutputStream());

            Usuario user = new Usuario();
            user.setCorreo(username);
            user.setPass(wp.hashear(password));
            user.setTipo("clubs");

            System.out.println("INTENTANDO ENTRAR CON " + username + " y " + password);

            String strSalida = "";
            strSalida = wp.cifrar(g.toJson(user), "123456789123456789");
            salida.writeUTF(strSalida);
            String entradaNueva = "";
            entradaNueva = entrada.readUTF();

            System.out.println("COMPROBAMOS....");
            if(entradaNueva.equalsIgnoreCase("si")){
                String entrada2 = entrada.readUTF();
                entrada2 = wp.desCifrar(entrada2, "123456789123456789");

                switch (user.getTipo()){
                    case "clubs":
                        Club club;
                        club = g.fromJson(entrada2, Club.class);
                        Variables.club = club;

                        byte[] bytes = new byte[62100];
                        entrada.read(bytes);
                        System.out.println("buen byte " + bytes);
                        if (bytes.length > 0){
                            System.out.println("----->" + bytes);
                            Variables.club.setFoto(bytes);
                        }


                        break;
                    case "entrenador":
                        Entrenador entrenador;
                        entrenador = g.fromJson(entrada2, Entrenador.class);
                        Variables.entrenador = entrenador;
                        break;
                    case "jugador":
                        Jugador jugador;
                        jugador = g.fromJson(entrada2, Jugador.class);
                        Variables.jugador = jugador;
                        break;
                    case "aficionado":
                        Aficionado aficionado;
                        aficionado = g.fromJson(entrada2, Aficionado.class);
                        Variables.aficionado = aficionado;
                        break;
                    default:
                        break;
                }
                entrada.close();
                salida.close();
                return true;
            }else{
                entrada.close();
                salida.close();
                return false;
            }
        }catch (Exception e){
            System.out.println("PETÓ-------------------------");
            e.printStackTrace();
            return false;
        }
    }
}