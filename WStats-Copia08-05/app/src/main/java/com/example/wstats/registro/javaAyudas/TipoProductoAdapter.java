package com.example.wstats.registro.javaAyudas;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import com.example.wstats.R;

import java.util.ArrayList;

/*
Crea el menu desplegable "Spinner" que establece la foto y el tipo de producto
 */
public class TipoProductoAdapter extends BaseAdapter{

    private Context context;
    private ArrayList<TiposProducto> tiposDeProducto;
    private LayoutInflater inflater;

/*
Constructor de la clase TipoProductoAdapter
 */
    public TipoProductoAdapter(Activity context, ArrayList<TiposProducto> tiposDeProducto){
        this.context = context;
        this.tiposDeProducto = tiposDeProducto;
        this.inflater = LayoutInflater.from(context);
    }
/*
Lo que necesita mostrar el holder
 */
    public static class ViewHolder{
        ImageView foto;
        TextView nombre;
    }

    /*
    Tamaño de la lista
     */
    @Override
    public int getCount() {
        return this.tiposDeProducto.size();
    }

    /*
    Posicion del item selecionado
     */
    @Override
    public Object getItem(int position) {
        return this.tiposDeProducto.get(position);
    }

    /*
    Getter de la id de la lista seleccionada
     */
    @Override
    public long getItemId(int position) {
        return position;
    }

    /*
    Metodo que permite la vista de los productos en el spinner que luego serán seleccionados
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView ==null){
            convertView = inflater.inflate(R.layout.lista, null);
            holder = new ViewHolder();
            holder.foto = (ImageView) convertView.findViewById(R.id.fotoJugadorList);
            holder.nombre = (TextView) convertView.findViewById(R.id.txtNombreCategoria);
            convertView.setTag(holder);
        }
        else{
            holder =(ViewHolder) convertView.getTag();
        }
        TiposProducto tipoProducto = tiposDeProducto.get(position);
        holder.foto.setImageBitmap(tipoProducto.getFoto());
        //holder.foto.setImageDrawable(context.getResources().getDrawable(R.drawable.carne));
        holder.nombre.setText(tipoProducto.getNombreProducto());

        return convertView;
    }
}

