package com.example.wstats.principal.ui.plantilla.jugadores;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.example.wstats.R;
import com.example.wstats.claseGlobal.Variables;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.ignacio.Club;
import com.ignacio.Jugador;
import com.ignacio.Usuario;
import com.nacho.WHashCrypt;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.Socket;
import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class club_jugadoresActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemLongClickListener{

    private ArrayList<Jugador> arrayList;
    private JugadorAdapter adapter;
    private Jugador jugadorSeleccionado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_club_jugadores);
        arrayList = new ArrayList<Jugador>();
        Button btn = findViewById(R.id.btnNuevoJugador);
        btn.setOnClickListener(this);

        ListView listView = (ListView) findViewById(R.id.listaClubJugadores);
        sacarDatosServidor();

        adapter = new JugadorAdapter(this, arrayList);
        listView.setAdapter(adapter);
        listView.setOnItemLongClickListener(this);
        registerForContextMenu(listView);
        System.out.println("yepee");

        TextView mombreClub = findViewById(R.id.txtNombreCategorias);
        mombreClub.setText(Variables.club.getNombre());
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        setContentView(R.layout.activity_club_jugadores);
        arrayList = new ArrayList<Jugador>();
        Button btn = findViewById(R.id.btnNuevoJugador);
        btn.setOnClickListener(this);

        ListView listView = (ListView) findViewById(R.id.listaClubJugadores);
        sacarDatosServidor();

        adapter = new JugadorAdapter(this, arrayList);
        listView.setAdapter(adapter);
        listView.setOnItemLongClickListener(this);
        registerForContextMenu(listView);
        System.out.println("yepee");

        TextView mombreClub = findViewById(R.id.txtNombreCategorias);
        mombreClub.setText(Variables.club.getNombre());
    }

    private void sacarDatosServidor() {
        try {
            WHashCrypt wp = new WHashCrypt();
            Gson g = new Gson();
            System.out.println("ENTRO A PROBAR");
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
            Socket socket = new Socket(Variables.IP_SERVER, Variables.PUERTO_SERVER);
            System.out.println("SERVER CONECTADO");
            // Declaramos un DataInputStream para leer los datos
            DataInputStream entrada = new DataInputStream(socket.getInputStream());

            // Declaramos un DataOutputStream para escribir los datos
            DataOutputStream salida = new DataOutputStream(socket.getOutputStream());
            String strEnvio = null;
            Usuario user = new Usuario();
            user.setTipo("sacarJugadoresClub");

            strEnvio = wp.cifrar(g.toJson(user), "123456789123456789");
            salida.writeUTF(strEnvio);

            //envio el club para pillar la id
            Club club = Variables.club;
            club.setFoto(new byte[0]);
            strEnvio = wp.cifrar(g.toJson(club), "123456789123456789");
            salida.writeUTF(strEnvio);

            // recibo el array
            String recibido = entrada.readUTF();
            recibido = wp.desCifrar(recibido, "123456789123456789");

            JsonArray arry = new JsonParser().parse(recibido).getAsJsonArray();
            arrayList.clear();
            for (JsonElement jsonElement : arry) {
                arrayList.add(g.fromJson(jsonElement, Jugador.class));
            }
            //adapter.notifyDataSetChanged();

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public boolean onContextItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()){

            case R.id.menu_contextual_eliminar:
                new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
                        .setTitleText("¿Estas seguro?")
                        .setContentText("¡Se eliminará este equipo!")
                        .setConfirmText("Eliminar")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                // reuse previous dialog instance
                                arrayList.remove(jugadorSeleccionado);
                                eliminarDatosServidor();
                                sDialog.setTitleText("¡Actualizado!")
                                        .setContentText("¡Tus datos se han modificado correctamente!")
                                        .setConfirmText("OK")
                                        .setConfirmClickListener(null)
                                        .changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
                            }
                        })
                        .show();
                break;
            case R.id.menu_contextual_detalles:
                Intent i = new Intent(this, Modificar_jugadorActivity.class);
                Gson g = new Gson();
                i.putExtra("jugador", g.toJson(jugadorSeleccionado));
                startActivity(i);
                break;
        }

        return super.onContextItemSelected(item);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        getMenuInflater().inflate(R.menu.menu_contextual_listajugadores, menu);
    }

    private void eliminarDatosServidor() {
        try {
            WHashCrypt wp = new WHashCrypt();
            Gson g = new Gson();
            System.out.println("ENTRO A PROBAR");
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
            Socket socket = new Socket(Variables.IP_SERVER, Variables.PUERTO_SERVER);
            System.out.println("SERVER CONECTADOssss");
            // Declaramos un DataInputStream para leer los datos
            DataInputStream entrada = new DataInputStream(socket.getInputStream());

            // Declaramos un DataOutputStream para escribir los datos
            DataOutputStream salida = new DataOutputStream(socket.getOutputStream());
            String strEnvio = null;
            Usuario user = new Usuario();
            user.setTipo("eliminarJugadoresClub");

            strEnvio = wp.cifrar(g.toJson(user), "123456789123456789");
            salida.writeUTF(strEnvio);

            //envio el club para pillar la id
            strEnvio = wp.cifrar(g.toJson(Variables.club), "123456789123456789");
            salida.writeUTF(strEnvio);

            //envio el arraylist para pillar
            salida.writeUTF(jugadorSeleccionado.getIdJugador()+"");

            // recibo el array actualizado
            String recibido = entrada.readUTF();
            recibido = wp.desCifrar(recibido, "123456789123456789");
            System.out.println("BORRÉ");
            arrayList.clear();
            JsonArray arry = new JsonParser().parse(recibido).getAsJsonArray();
            for (JsonElement jsonElement : arry) {
                arrayList.add(g.fromJson(jsonElement, Jugador.class));
            }
            adapter.notifyDataSetChanged();

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.btnNuevoJugador:
                Intent intent = new Intent(this, Crear_jugadorActivity.class);
                startActivity(intent);
                break;
        }
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        jugadorSeleccionado = arrayList.get(position);

        //categoriaSeleccionada = arrayList.get(position);
        return false;
    }
}