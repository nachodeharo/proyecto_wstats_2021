package com.example.wstats.principal.ui.plantilla.jugadores;

import android.app.Activity;
import android.content.Context;
import android.os.StrictMode;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import com.example.wstats.R;
import com.example.wstats.claseGlobal.Variables;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.ignacio.Club;
import com.ignacio.Equipo;
import com.ignacio.Jugador;
import com.ignacio.Usuario;
import com.nacho.WHashCrypt;

import org.w3c.dom.Text;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.Socket;
import java.util.ArrayList;

public class JugadorAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<Jugador> listaJugadores;
    private LayoutInflater inflater;
    private ArrayList<Equipo> arrayList;

    /*
    Constructor de la clase, se le pasa la activity y un arraylist
     */
    public JugadorAdapter(Activity context, ArrayList<Jugador> listaJugadores){
        this.context = context;
        this.listaJugadores = listaJugadores;
        this.inflater = LayoutInflater.from(context);
        arrayList = new ArrayList<>();
        sacarDatosServer();
    }

    /*
    Lo que va a llevar el holder
     */
    public static class ViewHolder{
        TextView nombre;
        TextView equipo;
        TextView posicion;
        TextView dni;
        ImageView imagen;
    }
    /*
    Getter del tamaño de la lista
     */
    @Override
    public int getCount() {
        return this.listaJugadores.size();
    }

    /*
    getter de la posicion de la lista
     */
    @Override
    public Object getItem(int position) {
        return this.listaJugadores.get(position);
    }

    /*
    Getter de la id de la lista
     */
    @Override
    public long getItemId(int position) {
        return position;
    }

    /*
    Permite insertar en la lista cada producto
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView ==null){
            convertView = inflater.inflate(R.layout.lista_jugador, null);
            holder = new ViewHolder();
            holder.nombre = (TextView) convertView.findViewById(R.id.txtNombreListaPj);
            holder.equipo = (TextView) convertView.findViewById(R.id.txtEquipoListaPj);
            holder.posicion = (TextView) convertView.findViewById(R.id.txtPosicionListaPj);
            holder.dni = (TextView) convertView.findViewById(R.id.txtDNIListaPj);
            holder.imagen = convertView.findViewById(R.id.fotoJugadorList);
            convertView.setTag(holder);
        }
        else{
            holder =(ViewHolder) convertView.getTag();
        }

        Jugador jugador = listaJugadores.get(position);
        System.out.println(jugador);
        holder.nombre.setText(jugador.getNombre() + " " + jugador.getApellidos());
        //holder.equipo.setText(jugador.getIdEquipo() +"");
        for (int i = 0 ; i < arrayList.size() ; i++){
            if (arrayList.get(i).getIdEquipo() == jugador.getIdEquipo()){
                holder.equipo.setText(arrayList.get(i).getNombreEquipo());
            }
        }
        holder.posicion.setText(jugador.getPosicion());
        holder.dni.setText(jugador.getDni());
        holder.imagen.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.entrenador));

        return convertView;
    }

    private void sacarDatosServer() {
        try {
            WHashCrypt wp = new WHashCrypt();
            Gson g = new Gson();
            System.out.println("ENTRO A PROBAR");
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
            Socket socket = new Socket(Variables.IP_SERVER, Variables.PUERTO_SERVER);
            System.out.println("SERVER CONECTADO");
            // Declaramos un DataInputStream para leer los datos
            DataInputStream entrada = new DataInputStream(socket.getInputStream());

            // Declaramos un DataOutputStream para escribir los datos
            DataOutputStream salida = new DataOutputStream(socket.getOutputStream());
            String strEnvio = null;
            Usuario user = new Usuario();
            user.setTipo("sacarEquiposClub");

            strEnvio = wp.cifrar(g.toJson(user), "123456789123456789");
            salida.writeUTF(strEnvio);

            //envio el club para pillar la id
            Club club = Variables.club;
            club.setFoto(new byte[0]);
            strEnvio = wp.cifrar(g.toJson(club), "123456789123456789");
            salida.writeUTF(strEnvio);

            // recibo el array
            String recibido = entrada.readUTF();
            recibido = wp.desCifrar(recibido, "123456789123456789");

            JsonArray arry = new JsonParser().parse(recibido).getAsJsonArray();
            arrayList.clear();
            for (JsonElement jsonElement : arry) {
                arrayList.add(g.fromJson(jsonElement, Equipo.class));
            }
            //adapter.notifyDataSetChanged();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}

