package com.example.wstats.registro;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.StrictMode;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.wstats.R;
import com.example.wstats.login.ui.LoginActivity;
import com.example.wstats.registro.javaAyudas.TipoProductoAdapter;
import com.example.wstats.registro.javaAyudas.TiposProducto;
import com.google.gson.Gson;
import com.ignacio.Aficionado;
import com.ignacio.Usuario;
import com.nacho.WHashCrypt;

import org.w3c.dom.Text;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.sql.Date;
import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemClickListener, AdapterView.OnItemSelectedListener {

    TextView textoLogeo;
    EditText fecha;
    Button registerButton;
    Date fechaNacimiento;
    TextView textoRegistro;
    private TipoProductoAdapter adapter;
    private ArrayList<TiposProducto> listaProductos;
    private TiposProducto tipoProducto;
    private int i = -1;
    private boolean correcto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        textoLogeo = findViewById(R.id.text_click);
        textoLogeo.setOnClickListener(this);

        registerButton = findViewById(R.id.btnRegistrarse);
        registerButton.setOnClickListener(this);

        fecha = findViewById(R.id.txtFechaNacRegistro);
        fecha.setOnClickListener(this);

        textoRegistro = findViewById(R.id.text_click);

        listaProductos = new ArrayList<TiposProducto>();
        ImageView imagen = null;
        Bitmap miBitMap= null;

        /*
        Creo el spinner para el desplegable
         */
        TiposProducto producto = new TiposProducto();
        TiposProducto producto2 = new TiposProducto();
        TiposProducto producto3 = new TiposProducto();
        TiposProducto producto4 = new TiposProducto();

        producto.setNombreProducto("Jugador");
        miBitMap= BitmapFactory.decodeResource(getResources(), R.drawable.jugador);
        producto.setFoto(miBitMap);
        listaProductos.add(producto);

        producto2.setNombreProducto("Entrenador");
        miBitMap= BitmapFactory.decodeResource(getResources(), R.drawable.entrenador);
        producto2.setFoto(miBitMap);
        listaProductos.add(producto2);

        producto3.setNombreProducto("Aficionado");
        miBitMap= BitmapFactory.decodeResource(getResources(), R.drawable.aficionado);
        producto3.setFoto(miBitMap);
        listaProductos.add(producto3);

        producto4.setNombreProducto("Club");
        miBitMap= BitmapFactory.decodeResource(getResources(), R.drawable.ball);
        producto4.setFoto(miBitMap);
        listaProductos.add(producto4);

         /*
        Los añado al spinner
         */
        Spinner lvLista = findViewById(R.id.spinner_register);
        adapter = new TipoProductoAdapter(this, listaProductos);
        lvLista.setAdapter(adapter);
        lvLista.setOnItemSelectedListener(this);


    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.text_click:
                crearActivity();
                break;

            case R.id.txtFechaNacRegistro:
                showDatePickerDialog();
                break;

            case R.id.btnRegistrarse:
                Aficionado registro = new Aficionado();
                EditText txt;
                if (comprobarCamposVacios()){
                    txt = findViewById(R.id.txtPasswordRegistro);
                    if (txt.getText().toString().length() > 5){
                        txt = findViewById(R.id.txtNombreRegistro);
                        registro.setNombre(txt.getText().toString());

                        txt = findViewById(R.id.txtApellidosRegistro);
                        registro.setApellidos(txt.getText().toString());

                        txt = findViewById(R.id.txtCiudadRegistro);
                        registro.setCiudad(txt.getText().toString());

                        txt = findViewById(R.id.txtCorreoRegistro);
                        registro.setCorreo(txt.getText().toString());

                        txt = findViewById(R.id.txtPasswordRegistro);
                        registro.setPass(txt.getText().toString());

                        txt = findViewById(R.id.txtFechaNacRegistro);
                        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
                        //System.out.println(LocalDate.parse(txt.getText().toString(), dateFormatter));
                        String fecha2 = txt.getText().toString();
                        LocalDate fecha = LocalDate.parse(fecha2, dateFormatter);
                        System.out.println(" Fecha: " + fecha);
                        registro.setFechanacimiento(fecha);

                        System.out.println(registro);

                        mostrarProgress();
                        enviarServidor(registro);
                    }else{
                        new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Contraseña inváida")
                                .setContentText("Debe tener >5 carácteres")
                                .show();
                    }
                }else{
                    new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Campos vacíos")
                            .setContentText("Relena todos los campos")
                            .show();
                }
                break;

        }

    }

    private void enviarServidor(Aficionado registro) {
        try {
            WHashCrypt wp = new WHashCrypt();
            Gson g = new Gson();
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
            System.out.println("ENTRO A PROBAR");
            Socket socket = new Socket("192.168.56.1", 4444);
            System.out.println("SERVER CONECTADO");
            // Declaramos un DataInputStream para leer los datos
            DataInputStream entrada = new DataInputStream(socket.getInputStream());

            // Declaramos un DataOutputStream para escribir los datos
            DataOutputStream salida = new DataOutputStream(socket.getOutputStream());

            String strEnvio = null;
            Usuario user = new Usuario();
            // PONER AQUI EL RESULTADO DEL COMBO
            user.setCorreo("aficionados");
            user.setTipo("registro");
            strEnvio = g.toJson(user);
            strEnvio = wp.cifrar(strEnvio, "123456789123456789");
            System.out.println("Envio paquete a server...");
            salida.writeUTF(strEnvio);
            
            // Envio los datos de registro
            strEnvio = g.toJson(registro);
            System.out.println("ENVIO ESTE JSON: " + strEnvio);
            strEnvio = wp.cifrar(strEnvio, "123456789123456789");
            salida.writeUTF(strEnvio);
            if(entrada.readUTF().equals("correcto")){
                correcto = true;
            }else{
                correcto = false;
                System.out.println("ERROR, EL DNI YA EXISTE");
            }

        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void mostrarProgress(){
        final SweetAlertDialog pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE)
                .setTitleText("Comprobando datos");
        pDialog.show();
        pDialog.setCancelable(false);
        new CountDownTimer(800 * 4, 800) {
            public void onTick(long millisUntilFinished) {
                // you can change the progress bar color by ProgressHelper every 800 millis
                i++;
                switch (i){
                    case 0:
                        pDialog.getProgressHelper().setBarColor(getResources().getColor(R.color.blue_btn_bg_color));
                        break;
                    case 1:
                        pDialog.getProgressHelper().setBarColor(getResources().getColor(R.color.material_deep_teal_50));
                        break;
                    case 2:
                        pDialog.getProgressHelper().setBarColor(getResources().getColor(R.color.success_stroke_color));
                        break;
                    case 3:
                        pDialog.getProgressHelper().setBarColor(getResources().getColor(R.color.material_deep_teal_20));
                        break;
                    case 4:
                        pDialog.getProgressHelper().setBarColor(getResources().getColor(R.color.material_blue_grey_80));
                        break;
                    case 5:
                        pDialog.getProgressHelper().setBarColor(getResources().getColor(R.color.warning_stroke_color));
                        break;
                    case 6:
                        pDialog.getProgressHelper().setBarColor(getResources().getColor(R.color.success_stroke_color));
                        break;
                }
            }

            public void onFinish() {
                i = -1;
                if (correcto){
                    pDialog.setTitleText("¡Registrado!")
                            .setConfirmText("OK")
                            .changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
                    crearActivity();
                }else{
                    pDialog.setTitleText("Oops...")
                            .setConfirmText("Error, el correo ya existe")
                            .changeAlertType(SweetAlertDialog.ERROR_TYPE);
                }
            }
        }.start();
    }
        private boolean comprobarCamposVacios() {
        RadioButton r1, r2;
        EditText nombre, apellidos, pass, ciudad, correo, fecha, hijo;

        r1 = findViewById(R.id.rBtnAficionado);
        r2 = findViewById(R.id.rBtnPadre);

        nombre = findViewById(R.id.txtNombreRegistro);
        apellidos = findViewById(R.id.txtApellidosRegistro);
        pass = findViewById(R.id.txtPasswordRegistro);
        ciudad = findViewById(R.id.txtCiudadRegistro);
        correo = findViewById(R.id.txtCorreoRegistro);
        fecha = findViewById(R.id.txtFechaNacRegistro);
        hijo = findViewById(R.id.txtHijoRegistro);

        if (nombre.getText().toString().isEmpty() || apellidos.getText().toString().isEmpty() ||
            pass.getText().toString().isEmpty() || ciudad.getText().toString().isEmpty() ||
            ciudad.getText().toString().isEmpty() || correo.getText().toString().isEmpty() ||
            correo.getText().toString().isEmpty() ||fecha.getText().toString().isEmpty()){

           return false;
        }

        if (r1.isSelected()){
            return true;
        }else{
            if (r2.isSelected() && hijo.getText().toString().isEmpty()){
                return false;
            }else{
                return true;
            }
        }

    }



    private void showDatePickerDialog() {
        DatePickerFragment newFragment = DatePickerFragment.newInstance(new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                // +1 because January is zero
                if ((month+1) < 10){
                    if (day < 10){
                        final String selectedDate = "0" + day + "/0" + (month+1) + "/" + year;
                        fecha.setText(selectedDate);
                    }else{
                        final String selectedDate = day + "/0" + (month+1) + "/" + year;
                        fecha.setText(selectedDate);
                    }

                }else {
                    final String selectedDate = day + "/" + (month + 1) + "/" + year;
                    fecha.setText(selectedDate);
                }
            }
        });

        newFragment.show(getSupportFragmentManager(), "datePicker");
    }

    private void crearActivity() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}